import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ApiRequests } from "../../../api/api.requests";
import { HeadingComponent } from "../../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { PageWhrapperComponent } from "../../../Components/PageWhrapper/PageWhrapperComponent";
import { ProfileBlock } from "../../../Components/DetailPage/ApplicantInResume/ProfileBlock";
import { ResumeBlock } from "../../../Components/DetailPage/Resume/ResumeBlock";
import Loader from "../../../styles/images/Loader/Spinner-1s-200px.svg";
import {
  pageWhrapperObj,
  headingPageBlockObj,
  headingPageObj,
  headingIsProfileObj,
  profileBlockObj,
  profileHeadingBlockObj,
  linkCreateProfileObj,
  headingIsCardsObj,
  headingCardBlockObj,
  cardsBlockObj,
} from "./Data";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";

export const DetailResumePage = () => {
  const { getApiData, loading } = ApiRequests();
  const [userInfo, setUserInfo] = useState(null);
  const [error, setError] = useState(null);
  const cardId = useParams().id;

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiData(setUserInfo, `/Resume/${cardId}`, setError);
  }, [getApiData, cardId]);

  useEffect(() => {
    if (error) {
      alert(error);
    }
    return () => {
      setError(null);
    };
  }, [error]);

  return (
    <PageWhrapperComponent {...pageWhrapperObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>

      <BlockComponent {...profileHeadingBlockObj}>
        <HeadingComponent {...headingIsProfileObj} />
      </BlockComponent>

      <BlockComponent {...profileBlockObj}>
        {loading && <Loader />}
        {!loading && userInfo && (
          <>
            {" "}
            <ProfileBlock userInfo={userInfo.applicant} />
            <LinkComponent
              {...linkCreateProfileObj}
              link={`/detail-applicant/${userInfo.applicant.id}`}
            />
          </>
        )}
      </BlockComponent>

      <BlockComponent {...headingCardBlockObj}>
        <HeadingComponent {...headingIsCardsObj} />
      </BlockComponent>

      <BlockComponent {...cardsBlockObj}>
        {loading && <Loader />}
        {!loading && userInfo && <ResumeBlock userInfo={userInfo} />}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
