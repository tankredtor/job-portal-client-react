export const pageWhrapperObj = {
  htmlName: "detail-card",
};

export const headingPageBlockObj = {
  htmlBlock: "heading-page",
};

export const headingPageObj = {
  htmlBlock: "heading-page__heading",
  typography: "heading-block-biggest",
  body: "Вакансия",
};

export const profileHeadingBlockObj = {
  htmlBlock: "heading-profile",
};

export const headingIsProfileObj = {
  htmlBlock: "heading-profile__heading",
  typography: "heading-block-big",
  body: "Профиль Организации, создавшей вакансию",
};

export const profileBlockObj = {
  htmlBlock: "block-profile",
};

export const headingCardBlockObj = {
  htmlBlock: "heading-cards",
};

export const headingNoCardsObj = {
  htmlBlock: "heading-cards__heading",
  typography: "heading-block-big",
  body: "У данного пользователя нет созданных резюме",
};

export const headingIsCardsObj = {
  htmlBlock: "heading-cards__heading",
  typography: "heading-block-big",
  body: "Информация о вакансии",
};

export const linkCreateProfileObj = {
  htmlBlock: "block-profile__button",
  htmlModel: "standart-button",
  description: "Подробнее",
};

export const cardsBlockObj = {
  htmlBlock: "block-cards",
};
