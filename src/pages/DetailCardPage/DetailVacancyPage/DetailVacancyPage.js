import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ApiRequests } from "../../../api/api.requests";
import { HeadingComponent } from "../../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { PageWhrapperComponent } from "../../../Components/PageWhrapper/PageWhrapperComponent";
import { ProfileBlock } from "../../../Components/DetailPage/EmployerInVacancy/ProfileBlock";
import { VacancyBlock } from "../../../Components/DetailPage/Vacancy/VacancyBlock";
import Loader from "../../../styles/images/Loader/Spinner-1s-200px.svg";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import {
  pageWhrapperObj,
  headingPageBlockObj,
  headingPageObj,
  headingIsProfileObj,
  profileBlockObj,
  profileHeadingBlockObj,
  linkCreateProfileObj,
  headingIsCardsObj,
  headingCardBlockObj,
  cardsBlockObj,
} from "./Data";

export const DetailVacancyPage = () => {
  const { getApiData, loading } = ApiRequests();
  const [userInfo, setUserInfo] = useState(null);
  const [error, setError] = useState(null);
  const cardId = useParams().id;

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiData(setUserInfo, `/Vacancy/${cardId}`, setError);
  }, [getApiData, cardId]);

  useEffect(() => {
    if (error) {
      alert(error);
    }
    return () => {
      setError(null);
    };
  }, [error]);

  return (
    <PageWhrapperComponent {...pageWhrapperObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>

      <BlockComponent {...profileHeadingBlockObj}>
        <HeadingComponent {...headingIsProfileObj} />
      </BlockComponent>

      <BlockComponent {...profileBlockObj}>
        {loading && <Loader />}
        {!loading && userInfo && (
          <>
            {" "}
            <ProfileBlock userInfo={userInfo.employer} />
            <LinkComponent
              {...linkCreateProfileObj}
              link={`/detail-employer/${userInfo.employer.id}`}
            />{" "}
          </>
        )}
      </BlockComponent>

      <BlockComponent {...headingCardBlockObj}>
        <HeadingComponent {...headingIsCardsObj} />
      </BlockComponent>

      <BlockComponent {...cardsBlockObj}>
        {loading && <Loader />}
        {!loading && userInfo && <VacancyBlock userInfo={userInfo} />}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
