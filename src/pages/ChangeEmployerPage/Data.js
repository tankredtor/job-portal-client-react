export const formObjEmployerChange = {
  headingPrimary: " Введите данные для изменения профиля работодателя",
  headingSecondary:
    'Введенные Вами данные будут доступны для просмотра Соискателям при переходе по ссылке вашей вакансии в разделе "Вакансии". Убедитесь, что указываете актуальные данные, чтобы соискатель мог связаться с Вами и направить Резюме.',
};

export const confirmObj = {
  htmlBlock: "confirm-block",
};

export const buttonObjChange = {
  description: "Изменить",
  htmlName: "change-employer",
};
export const buttonObjDelete = {
  description: "Удалить",
  htmlName: "delete-employer",
};

export const textAreaObjNameEmployer = {
  heading: "Название организациии",
  type: "text",
  placeholder: "Ваша организация",
  name: "name",
  size: "small",
};

export const textAreaObjDescriptionEmployer = {
  heading: "Расскажите о вашей деятельности",
  type: "text",
  placeholder: "Чем занимается ваша организация?",
  name: "description",
  size: "big",
};

export const inputObjInfoUrlEmployer = {
  heading: "Ссылка на ваш сайт",
  type: "text",
  placeholder: "URL",
  name: "infoUrl",
  size: "small",
};

export const inputObjContactPhoneEmployer = {
  heading: "Телефон для связи",
  type: "number",
  placeholder: "Телефон",
  name: "contactPhone",
  size: "small",
};

export const inputObjContactEmailEmployer = {
  heading: "E-mail для связи",
  type: "text",
  placeholder: "E-mail",
  name: "contactEmail",
  size: "small",
};
