import React, { useCallback, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import useForm from "../../hooks/useForm";
import { useHistory } from "react-router-dom";
import { ApiRequests } from "../../api/api.requests";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import { AiFillDelete } from "react-icons/ai";
import { ConfirmBannerComponent } from "../../Components/Banner/ConfirmBannerComponent/ConfirmBannerComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import {
  formObjEmployerChange,
  buttonObjChange,
  textAreaObjNameEmployer,
  textAreaObjDescriptionEmployer,
  inputObjInfoUrlEmployer,
  inputObjContactPhoneEmployer,
  inputObjContactEmailEmployer,
  confirmObj,
} from "./Data";
import validate from "./validate";

export const ChangeEmployerPage = () => {
  const { getApiData, putApiData, deleteApiData, loading } = ApiRequests();
  const [form, setForm] = useState({
    name: "",
    description: "",
    infoUrl: "",
    contactEmail: "",
    contactPhone: "",
  });
  const employerId = useParams().id;
  const history = useHistory();
  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate
  );
  const [confirmShow, setConfirmShow] = useState(false);
  const changeButton = useCallback(async () => {
    await putApiData(
      {
        ...form,
      },
      `/Employer`
    );
    history.push(`/my-page`);
  }, [form, putApiData, history]);

  const deleteButton = async (event) => {
    event.preventDefault();
    await deleteApiData(`/Employer/${employerId}`);
    history.push(`/my-page`);
  };
  const onConfirmShow = (e) => {
    e.preventDefault();
    setConfirmShow(!confirmShow);
  };
  useEffect(() => {
    getApiData(setForm, `/Employer/${employerId}`);
  }, [getApiData, employerId]);

  useEffect(() => {
    if (isSubmitted) {
      changeButton();
    }
  }, [isSubmitted, changeButton]);

  return (
    <>
      {!loading && form && (
        <div className="create-employer-page">
          <FormComponent {...formObjEmployerChange}>
            <BlockComponent {...confirmObj}>
              {!confirmShow && (
                <AiFillDelete
                  onClick={onConfirmShow}
                  className="delete-icon"
                  size="30"
                />
              )}
              {confirmShow && (
                <ConfirmBannerComponent
                  confirm={deleteButton}
                  refusal={onConfirmShow}
                />
              )}
            </BlockComponent>

            <TextAreaElement
              {...textAreaObjNameEmployer}
              value={form.name}
              onChange={changeHandler}
            >
              {errors.name && <ErrorElement error={errors.name} />}
            </TextAreaElement>
            <TextAreaElement
              {...textAreaObjDescriptionEmployer}
              value={form.description}
              onChange={changeHandler}
            >
              {errors.description && (
                <ErrorElement error={errors.description} />
              )}
            </TextAreaElement>
            <InputElement
              {...inputObjInfoUrlEmployer}
              value={form.infoUrl}
              onChange={changeHandler}
            >
              {errors.infoUrl && <ErrorElement error={errors.infoUrl} />}
            </InputElement>

            <InputElement
              {...inputObjContactPhoneEmployer}
              value={form.contactPhone}
              onChange={changeHandler}
            >
              {errors.contactPhone && (
                <ErrorElement error={errors.contactPhone} />
              )}
            </InputElement>
            <InputElement
              {...inputObjContactEmailEmployer}
              value={form.contactEmail}
              onChange={changeHandler}
            >
              {errors.contactEmail && (
                <ErrorElement error={errors.contactEmail} />
              )}
            </InputElement>
            <ButtonStandartComponent
              {...buttonObjChange}
              onClick={handleSubmit}
            />
          </FormComponent>
        </div>
      )}
    </>
  );
};
