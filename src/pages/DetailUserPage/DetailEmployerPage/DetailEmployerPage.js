import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { ApiRequests } from "../../../api/api.requests";
import { HeadingComponent } from "../../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { PageWhrapperComponent } from "../../../Components/PageWhrapper/PageWhrapperComponent";
import { ProfileBlock } from "../../../Components/DetailPage/EmployerInEmployer/ProfileBlock";
import { SpanComponent } from "../../../Components/ReusableComponents/PresenatationComponents/SpanComponent";
import { CardsComponent } from "../CardsComponent/CardsComponent";
import Loader from "../../../styles/images/Loader/Spinner-1s-200px.svg";
import {
  pageWhrapperObj,
  headingPageBlockObj,
  headingPageObj,
  headingIsProfileObj,
  profileBlockObj,
  profileHeadingBlockObj,
  headingIsCardsObj,
  headingCardBlockObj,
  cardsBlockObj,
  spanNoCardsObj,
  spanIsCardsObj,
} from "./Data";

export const DetailEmployerPage = () => {
  const { getApiData, loading } = ApiRequests();
  const [userInfo, setUserInfo] = useState(null);
  const [error, setError] = useState(null);
  const userId = useParams().id;

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiData(setUserInfo, `/Employer/${userId}`, setError);
  }, [getApiData, userId]);

  useEffect(() => {
    if (error) {
      alert(error);
    }
    return () => {
      setError(null);
    };
  }, [error]);

  return (
    <PageWhrapperComponent {...pageWhrapperObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>

      <BlockComponent {...profileHeadingBlockObj}>
        <HeadingComponent {...headingIsProfileObj} />
      </BlockComponent>

      <BlockComponent {...profileBlockObj}>
        {loading && <Loader />}
        {!loading && userInfo && (
          <>
            {" "}
            <ProfileBlock userInfo={userInfo} />
          </>
        )}
      </BlockComponent>

      <BlockComponent {...headingCardBlockObj}>
        <HeadingComponent {...headingIsCardsObj} />
      </BlockComponent>

      <BlockComponent {...cardsBlockObj}>
        {loading && <Loader />}

        {!loading && userInfo && userInfo.vacancies.length !== 0 && (
          <>
            {" "}
            <SpanComponent {...spanIsCardsObj} />
            {userInfo.vacancies.map((data) => {
              return (
                <CardsComponent
                  key={data.id}
                  data={data}
                  urlDetail={"/detail-vacancy/"}
                />
              );
            })}
          </>
        )}

        {!loading && userInfo && userInfo.vacancies.length === 0 && (
          <SpanComponent {...spanNoCardsObj} />
        )}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
