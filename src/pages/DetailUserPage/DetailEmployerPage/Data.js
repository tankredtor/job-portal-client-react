export const pageWhrapperObj = {
  htmlName: "detail-user",
};

export const headingPageBlockObj = {
  htmlBlock: "heading-page",
};

export const headingPageObj = {
  htmlBlock: "heading-page__heading",
  typography: "heading-block-biggest",
  body: "Работодатель",
};

export const profileHeadingBlockObj = {
  htmlBlock: "heading-profile",
};

export const headingIsProfileObj = {
  htmlBlock: "heading-profile__heading",
  typography: "heading-block-big",
  body: "Информация о Работодателе",
};

export const profileBlockObj = {
  htmlBlock: "block-profile",
};

export const headingCardBlockObj = {
  htmlBlock: "heading-cards",
};

export const headingNoCardsObj = {
  htmlBlock: "heading-cards__heading",
  typography: "heading-block-big",
  body: "У данного работодателя нет созданных вакансий",
};

export const headingIsCardsObj = {
  htmlBlock: "heading-cards__heading",
  typography: "heading-block-big",
  body: "Вакансии работодателя",
};

export const cardsBlockObj = {
  htmlBlock: "block-cards",
};
export const spanNoCardsObj = {
  htmlBlock: "block-cards__span",
  typography: "heading-sub-information",
  body: "У данного работодателя нет созданных вакансий",
};

export const spanIsCardsObj = {
  htmlBlock: "block-cards__span__is-cards",
  typography: "heading-block-medium",
  body: "Cписок вакансий работодателя",
};
