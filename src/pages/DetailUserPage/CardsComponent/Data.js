export const cardsBlockObj = {
  htmlBlock: "user-card",
};

export const cardsLeftBlockObj = {
  htmlBlock: "user-card__left",
};

export const cardsRightBlockObj = {
  htmlBlock: "user-card__right",
};

export const spanCardsNameObj = {
  htmlBlock: "user-card__data__name ",
  typography: "heading-block-big",
};

export const spanCardsSalaryObj = {
  htmlBlock: "user-card__data__salary ",
  typography: "heading-sub-information",
};

export const spanCardsDateObj = {
  htmlBlock: "user-card__data__date ",
  typography: "heading-sub-information",
};

export const linkCardsDetailObj = {
  htmlBlock: "user-card__data",
};
