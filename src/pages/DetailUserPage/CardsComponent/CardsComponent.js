import React from "react";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { SpanComponent } from "../../../Components/ReusableComponents/PresenatationComponents/SpanComponent";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import {
  cardsBlockObj,
  cardsLeftBlockObj,
  cardsRightBlockObj,
  spanCardsNameObj,
  spanCardsDateObj,
  spanCardsSalaryObj,
  linkCardsDetailObj,
} from "./Data";

export const CardsComponent = ({ data, urlDetail }) => {
  const limitTitle = (title, limit = 20) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split(" ").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join(" ")}...`;
    }
    return title;
  };

  const salaryInfo = () => {
    if (data.salaryFrom && data.salaryTo) {
      return `${data.salaryFrom} - ${data.salaryTo} Руб. `;
    } else if (data.salaryFrom && !data.salaryTo) {
      return `от ${data.salaryFrom} Руб.`;
    } else if (data.salaryTo && !data.salaryFrom) {
      return ` до ${data.salaryTo} Руб.`;
    } else if (data.salaryTo === 0 && data.salaryTo === data.salaryFrom) {
      return "Доход не указан";
    } else return "Доход не указан";
  };

  const dateOfCreation = (date) => {
    let today = new Date(date);
    const dd = String(today.getDate()).padStart(2, "0");
    const mm = String(today.getMonth() + 1).padStart(2, "0");
    const yyyy = today.getFullYear();

    today = dd + "." + mm + "." + yyyy;
    return today;
  };

  return (
    <BlockComponent {...cardsBlockObj}>
      <LinkComponent {...linkCardsDetailObj} link={`${urlDetail}${data.id}`}>
        <BlockComponent {...cardsLeftBlockObj}>
          <SpanComponent {...spanCardsNameObj} body={limitTitle(data.name)} />
          <SpanComponent {...spanCardsSalaryObj} body={salaryInfo()} />
        </BlockComponent>
        <BlockComponent {...cardsRightBlockObj}>
          {data.updatedAt && (
            <SpanComponent
              {...spanCardsDateObj}
              body={`Вакасия создана ${dateOfCreation(data.updatedAt)}`}
            />
          )}
        </BlockComponent>
      </LinkComponent>
    </BlockComponent>
  );
};
