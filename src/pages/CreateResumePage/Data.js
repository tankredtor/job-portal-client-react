export const formObjResumeCreate = {
  headingPrimary: " Введите данные для создания резюме",
  headingSecondary:
    'Введенные Вами данные будут доступны для просмотра работодателям в разделе "Резюме". Убедитесь, что в Вашем профиле соискателя указаны актуальные контактные данные, чтобы потенциальный работодатель мог связаться с Вами и направить предложение.',
};

export const buttonObj = {
  description: "Создать резюме",
  htmlName: "create-Resume",
};

export const inputObjNameResume = {
  heading: "Желаемая должность",
  type: "text",
  placeholder: "Кем вы хотели бы работать?",
  name: "name",
  size: "small",
};

export const inputObjSalaryFromResume = {
  heading: "Хотели бы доход от:",
  type: "number",
  placeholder: "0",
  name: "salaryFrom",
  size: "small",
};

export const inputObjSalaryToResume = {
  heading: "Хотели бы доход до:",
  type: "number",
  placeholder: "0",
  name: "salaryTo",
  size: "small",
};

export const textAreaObjAboutMeResume = {
  heading: "Расскажите о себе и своем опыте работы",
  type: "text",
  placeholder: "Рассказ о себе...",
  name: "aboutMe",
  size: "big",
};

export const inputObjExperienceResume = {
  heading: "Совокупный опыт работы",
  type: "number",
  placeholder: "Сколько лет?",
  name: "experience",
  size: "small",
};

export const selectObjEducationStatusResume = {
  heading: "На каком уровне образования вы находитесь на данный момент?",
  type: "number",
  name: "educationStatus",
};

export const optionObjBachelorNow = {
  type: "number",
  value: 0,
  description: "Обучаюсь на бакалавриате",
};

export const optionObjIsBachelorNoMaster = {
  type: "number",
  value: 1,
  description: "Окончил бакалавриат, (не поступал в магистратуру)",
};

export const optionObjSpecialistNow = {
  type: "number",
  value: 2,
  description: "Обучаюсь на специалитете",
};

export const optionObjIsSpecialist = {
  type: "number",
  value: 3,
  description: "Окончил специалитет",
};

export const optionObjMasterNow = {
  value: 4,
  description: "Обучаюсь в магистратуре",
};

export const optionObjIsMaster = {
  type: "number",
  value: 5,
  description: "Окончил магистратуру",
};

export const optionObjPG = {
  type: "number",
  value: 6,
  description: "Аспирант",
};

export const optionObjAD = {
  type: "number",
  value: 7,
  description: "Имею ученую степень",
};

export const selectObjExperienceResume = {
  heading: "Ваш совокупный опыт работы",
  type: "number",
  name: "experience",
};

export const optionObjNoExperience = {
  type: "number",
  value: 0,
  description: "Без опыта",
};

export const optionObjToThreeYears = {
  type: "number",
  value: 1,
  description: "От 1 года до 3 лет",
};

export const optionObjToSixYears = {
  type: "number",
  value: 2,
  description: "От 3 до 6 лет",
};

export const optionObjOverSixYears = {
  type: "number",
  value: 3,
  description: "Более 6 лет",
};
