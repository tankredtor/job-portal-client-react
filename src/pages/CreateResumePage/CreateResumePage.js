import React, { useState, useCallback, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ApiRequests } from "../../api/api.requests";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import useForm from "../../hooks/useForm";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import validate from "./validate";
import {
  formObjResumeCreate,
  buttonObj,
  inputObjNameResume,
  inputObjSalaryFromResume,
  inputObjSalaryToResume,
  textAreaObjAboutMeResume,
  selectObjEducationStatusResume,
  optionObjAD,
  optionObjBachelorNow,
  optionObjIsBachelorNoMaster,
  optionObjIsMaster,
  optionObjIsSpecialist,
  optionObjMasterNow,
  optionObjPG,
  optionObjSpecialistNow,
  selectObjExperienceResume,
  optionObjNoExperience,
  optionObjOverSixYears,
  optionObjToThreeYears,
  optionObjToSixYears,
} from "./Data";

export const CreateResumePage = () => {
  const { postApiData } = ApiRequests();
  const history = useHistory();
  const [form, setForm] = useState({
    name: "",
    aboutMe: "",
  });
  const [formNumber, setFormNumber] = useState({
    educationStatus: 0,
    experience: 0,
    salaryFrom: null,
    salaryTo: null,
  });
  const {
    handleSubmit,
    errors,
    isSubmitted,
    changeHandler,
    changeHandlerNumber,
  } = useForm(form, setForm, validate, formNumber, setFormNumber);

  const createButton = useCallback(async () => {
    await postApiData({ ...form, ...formNumber }, "/Resume");
    history.push(`/my-page`);
  }, [postApiData, form, formNumber, history]);

  useEffect(() => {
    if (isSubmitted) {
      createButton();
    }
  }, [isSubmitted, createButton]);

  return (
    <div className="create-resume-page">
      <FormComponent {...formObjResumeCreate}>
        <InputElement
          {...inputObjNameResume}
          value={form.name || ""}
          onChange={changeHandler}
        >
          {" "}
          {errors.name && <ErrorElement error={errors.name} />}
        </InputElement>

        <TextAreaElement
          {...textAreaObjAboutMeResume}
          value={form.aboutMe || ""}
          onChange={changeHandler}
        >
          {errors.aboutMe && <ErrorElement error={errors.aboutMe} />}
        </TextAreaElement>

        <SelectElement
          {...selectObjEducationStatusResume}
          value={formNumber.educationStatus}
          onChange={changeHandlerNumber}
        >
          <OptionElement {...optionObjBachelorNow} />
          <OptionElement {...optionObjIsBachelorNoMaster} />
          <OptionElement {...optionObjSpecialistNow} />
          <OptionElement {...optionObjIsSpecialist} />
          <OptionElement {...optionObjMasterNow} />
          <OptionElement {...optionObjIsMaster} />
          <OptionElement {...optionObjPG} />
          <OptionElement {...optionObjAD} />
        </SelectElement>

        <SelectElement
          {...selectObjExperienceResume}
          value={formNumber.experience}
          onChange={changeHandlerNumber}
        >
          <OptionElement {...optionObjNoExperience} />
          <OptionElement {...optionObjToThreeYears} />
          <OptionElement {...optionObjToSixYears} />
          <OptionElement {...optionObjOverSixYears} />
        </SelectElement>

        <InputElement
          {...inputObjSalaryFromResume}
          value={formNumber.salaryFrom || ""}
          onChange={changeHandlerNumber}
        >
          {errors.salaryFrom && <ErrorElement error={errors.salaryFrom} />}
        </InputElement>

        <InputElement
          {...inputObjSalaryToResume}
          value={formNumber.salaryTo || ""}
          onChange={changeHandlerNumber}
        >
          {errors.salaryTo && <ErrorElement error={errors.salaryTo} />}{" "}
        </InputElement>

        <ButtonStandartComponent {...buttonObj} onClick={handleSubmit} />
      </FormComponent>
    </div>
  );
};
