export default function validateInfo(values, valuesNumber) {
  let errors = {};

  if (!values.name) {
    errors.name = "Вы не указали желаемую должность";
  }
  if (values.name.length > 64) {
    errors.name = "Ограничение на ввод 64 символа";
  }
  if (!values.aboutMe) {
    errors.aboutMe = "Вы не рассказали о себе";
  }
  if (values.aboutMe.length > 1024) {
    errors.aboutMe = "Ограничение на ввод 1024 символа";
  }

  if (
    valuesNumber.salaryTo &&
    valuesNumber.salaryFrom > valuesNumber.salaryTo
  ) {
    errors.salaryFrom = '"Доход от" не может быть больше "Доход до" ';
  }
  if (valuesNumber.salaryFrom && String(valuesNumber.salaryFrom).length > 10) {
    errors.salaryFrom = "Ограничение на ввод 10 чисел ";
  }
  if (valuesNumber.salaryTo && String(valuesNumber.salaryTo).length > 10) {
    errors.salaryTo = "Ограничение на ввод 10 чисел";
  }
  console.log(String(valuesNumber.salaryTo).length);
  return errors;
}
