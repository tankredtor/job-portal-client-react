import React, { useCallback, useEffect, useState } from "react";
import { ApiRequests } from "../../api/api.requests";
import { ResumesBlockSearchShow } from "../../Components/SearchPage/ResumesBlock/ResumesBlockSearchShow";
import { SortingBarComponent } from "../../Components/SearchPage/SortingBar/SortingBarComponent";
import { NoResume } from "../../Components/SearchPage/ResumesBlock/NoResume";
import { PaginationSizeComponent } from "../../Components/SearchPage/PaginationSize/PaginationSizeComponent";
import { PaginationButtonsComponent } from "../../Components/SearchPage/PaginationButtons/PaginationButtonsComponent";
import { FilterSearchComponent } from "../../Components/SearchPage/FilterBar/FilterSearchComponent ";
import { PageWhrapperComponent } from "../../Components/PageWhrapper/PageWhrapperComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import { InputComponent } from "../../Components/ReusableComponents/ActiveComponents/InputComponent";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { BiSearchAlt } from "react-icons/bi";
import { HeadingComponent } from "../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import Loader from "../../styles/images/Loader/Spinner-1s-200px.svg";
import {
  searchPageObj,
  searchBlockObj,
  searchSearchFieldBlockObj,
  paginationSizeBlockObj,
  cardsBlockObj,
  paginationButtonsUpperBlockObj,
  paginationButtonsLowerBlockObj,
  FilterBlockObj,
  inputSearchObj,
  buttonShowFilterObj,
  FilterMobileBlockObj,
  headingPageBlockObj,
  headingPageObj,
} from "./Data";

export const SearchPageResume = () => {
  const { getApiDataFilterResumes, loading } = ApiRequests();

  const [resumes, setResumes] = useState(null);
  const [filteringName, setFilteringName] = useState("");
  const [filteringSalaryFrom, setFilteringSalaryFrom] = useState("");
  const [filteringSalaryTo, setFilteringSalaryTo] = useState("");
  const [filteringExperience, setFilteringExperience] = useState("");
  const [sortingByAscending, setSortingByAscending] = useState(false);
  const [sortingFieldName, setSortingFieldName] = useState("");
  const [paramsPaginationPageSize, setParamsPaginationPageSize] = useState(
    null
  );
  const [reloadFilter, setReloadFIlter] = useState(false);
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(null);
  const [sorting, setSorting] = useState(false);
  const [showFilter, setShowFilter] = useState(false);

  const onEnterPress = (event) => {
    if (event.key === "Enter") {
      searchHandler();
    }
  };

  //filter button
  const filterButton = (e) => {
    e.preventDefault();
    // filter();
  };

  const pressSearchHandler = (e) => {
    e.preventDefault();
    searchHandler();
  };

  const buttonShowFilter = (e) => {
    e.preventDefault();
    setShowFilter(!showFilter);
  };
  //////////////////////////////First render//////////////////////////////

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume");
  }, [getApiDataFilterResumes]);

  //////////////////////////////Search and Filter//////////////////////////////
  const searchHandler = useCallback(() => {
    getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume", {
      "Filtering.Name": filteringName,
      "Filtering.SalaryFrom": filteringSalaryFrom,
      "Filtering.SalaryTo": filteringSalaryTo,
      "Sorting.SortingField": sortingFieldName,
      "Sorting.ByAscending": sortingByAscending,
      "Filtering.Experience": filteringExperience,
      "Pagination.PageSize": paramsPaginationPageSize,
    });
  }, [
    getApiDataFilterResumes,
    filteringSalaryFrom,
    filteringSalaryTo,
    sortingFieldName,
    filteringName,
    sortingByAscending,
    filteringExperience,
    paramsPaginationPageSize,
  ]);

  //////////////////////////////Sorting//////////////////////////////

  useEffect(() => {
    if (sorting) {
      getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume", {
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Filtering.Experience": filteringExperience,
        "Pagination.PageSize": paramsPaginationPageSize,
      });
    }
    return () => {
      setSorting(false);
    };
  }, [sortingByAscending, sortingFieldName, getApiDataFilterResumes]);

  //////////////////////////////Page Switch //////////////////////////////

  const paginationSwitchHandler = useCallback(
    (pagination) => {
      getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume", {
        "Pagination.Page": pagination,
        "Pagination.PageSize": paramsPaginationPageSize,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.Experience": filteringExperience,
      });
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    },
    [
      getApiDataFilterResumes,
      paramsPaginationPageSize,
      filteringName,
      filteringSalaryFrom,
      filteringSalaryTo,
      sortingByAscending,
      sortingFieldName,
      filteringExperience,
    ]
  );

  //////////////////////////////Page Size//////////////////////////////

  const paginationSizeHandler = useCallback(
    (size) => {
      getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume", {
        "Pagination.Page": page,
        "Pagination.PageSize": size,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.Experience": filteringExperience,
      });
    },
    [
      getApiDataFilterResumes,
      filteringName,
      page,
      filteringSalaryFrom,
      filteringSalaryTo,
      sortingByAscending,
      sortingFieldName,
      filteringExperience,
    ]
  );

  ////////////////////////////// Reloading Filter //////////////////////////////

  useEffect(() => {
    if (reloadFilter) {
      setFilteringName("");
      setFilteringSalaryFrom(null);
      setFilteringSalaryTo(null);
      setSortingByAscending(false);
      setSortingFieldName(null);
      setFilteringExperience(null);
      getApiDataFilterResumes(setResumes, setPage, setPages, "/Resume", {
        "Pagination.PageSize": paramsPaginationPageSize,
      });
    }

    return () => {
      setReloadFIlter(false);
    };
  }, [reloadFilter, getApiDataFilterResumes]);

  return (
    <PageWhrapperComponent {...searchPageObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>
      <BlockComponent {...searchBlockObj}>
        <BlockComponent {...searchSearchFieldBlockObj}>
          <InputComponent
            {...inputSearchObj}
            value={filteringName}
            onChange={(e) => setFilteringName(e.target.value)}
            onKeyPress={onEnterPress}
          />
          <BiSearchAlt
            className="search-block__search__icon"
            size="40"
            onClick={pressSearchHandler}
          />
        </BlockComponent>

        <SortingBarComponent
          setSortingFieldName={setSortingFieldName}
          setSortingByAscending={setSortingByAscending}
          setSorting={setSorting}
        />
      </BlockComponent>

      <BlockComponent {...paginationButtonsUpperBlockObj}>
        {!loading && resumes && (
          <PaginationButtonsComponent
            paginationSwitchHandler={paginationSwitchHandler}
            page={page}
            pages={pages}
          />
        )}
      </BlockComponent>
      <BlockComponent {...cardsBlockObj}>
        {loading && <Loader />}
        {!loading && resumes && <ResumesBlockSearchShow resumes={resumes} />}
        {!resumes && !loading && <NoResume />}
      </BlockComponent>

      <BlockComponent {...paginationSizeBlockObj}>
        {!loading && resumes && (
          <PaginationSizeComponent
            paginationSizeHandler={paginationSizeHandler}
            setParamsPaginationPageSize={setParamsPaginationPageSize}
          />
        )}
      </BlockComponent>

      <BlockComponent {...paginationButtonsLowerBlockObj}>
        {!loading && resumes && (
          <PaginationButtonsComponent
            paginationSwitchHandler={paginationSwitchHandler}
            page={page}
            pages={pages}
          />
        )}
      </BlockComponent>
      <BlockComponent {...FilterBlockObj}>
        <FilterSearchComponent
          valueSalaryFrom={filteringSalaryFrom}
          salaryFromChangeFunc={setFilteringSalaryFrom}
          valueSalaryTo={filteringSalaryTo}
          salaryToChangeFunc={setFilteringSalaryTo}
          valueFilteringRequiredExperience={filteringExperience}
          filterButton={filterButton}
          setFilteringRequiredExperience={setFilteringExperience}
          searchHandler={searchHandler}
          setReloadFilter={setReloadFIlter}
        />
      </BlockComponent>

      <BlockComponent {...FilterMobileBlockObj}>
        {!showFilter && (
          <ButtonStandartComponent
            {...buttonShowFilterObj}
            onClick={buttonShowFilter}
          />
        )}

        {showFilter && (
          <FilterSearchComponent
            valueSalaryFrom={filteringSalaryFrom}
            salaryFromChangeFunc={setFilteringSalaryFrom}
            valueSalaryTo={filteringSalaryTo}
            salaryToChangeFunc={setFilteringSalaryTo}
            valueFilteringRequiredExperience={filteringExperience}
            filterButton={filterButton}
            setFilteringRequiredExperience={setFilteringExperience}
            searchHandler={searchHandler}
            setReloadFilter={setReloadFIlter}
            showFilter={buttonShowFilter}
          />
        )}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
