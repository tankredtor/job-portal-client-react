export const pageWhrapperObj = {
  htmlName: "main-profile",
};

export const headingPageBlockObj = {
  htmlBlock: "heading-page",
};

export const headingPageObj = {
  htmlBlock: "heading-page__heading",
  typography: "heading-block-biggest",
  body: "Моя страница",
};

export const profileHeadingBlockObj = {
  htmlBlock: "heading-my-profile",
};

export const headingIsProfileObj = {
  htmlBlock: "heading-my-profile__heading",
  typography: "heading-block-big",
  body: "Профиль Организации",
};

export const headingNoProfileObj = {
  htmlBlock: "heading-my-profile__heading",
  typography: "heading-block-big",
  body: "Вы должны создать профиль",
};

export const profileBlockObj = {
  htmlBlock: "block-my-profile",
};

export const spanNoProfileObj = {
  htmlBlock: "block-my-profile__span",
  typography: "heading-block-medium",
  body: "После создания профиля, Вы сможете создать первую вакансию",
};

export const linkCreateProfileObj = {
  link: "/create-employer",
  htmlBlock: "block-my-profile__button",
  htmlModel: "standart-button",
  description: "Создать",
  htmlName: "block-my-profile__button",
};

export const linkChangeProfileObj = {
  htmlBlock: "block-my-profile__button",
  htmlModel: "standart-button",
  description: "Изменить",
};

export const cardsHeadingBlockObj = {
  htmlBlock: "heading-my-cards",
};

export const headingNoCardsObj = {
  htmlBlock: "heading-my-cards__heading",
  typography: "heading-block-big",
  body: "Здесь будут отображаться ваши вакансии",
};

export const headingCreateCardsObj = {
  htmlBlock: "heading-my-cards__heading",
  typography: "heading-block-big",
  body: "Создайте Вакансию",
};

export const headingIsCardsObj = {
  htmlBlock: "heading-my-cards__heading",
  typography: "heading-block-big",
  body: "вакансии организации",
};

export const cardsBlockObj = {
  htmlBlock: "block-my-cards",
};

export const spanNoCardsObj = {
  htmlBlock: "block-my-cards__span",
  typography: "heading-block-medium",
  body: "Нет созданных вакансий...",
};

export const linkCreateCardsObj = {
  link: "/create-vacancy",
  htmlBlock: "block-my-cards__button",
  htmlModel: "standart-button",
  description: "Создать Вакансию",
};

export const cardsListBlockObj = {
  htmlBlock: "block-my-cards__list",
};

export const spanNoCardsListObj = {
  htmlBlock: "block-my-cards__list__span",
  typography: "heading-block-medium",
  body: "Нет созданных вакансий...",
};
