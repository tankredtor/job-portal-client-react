import React, { useContext, useEffect, useState } from "react";
import { ApiRequests } from "../../../api/api.requests";
import { AuthContext } from "../../../context/AuthContext";
import { PageWhrapperComponent } from "../../../Components/PageWhrapper/PageWhrapperComponent";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { SpanComponent } from "../../../Components/ReusableComponents/PresenatationComponents/SpanComponent";
import { HeadingComponent } from "../../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import { FaArrowDown } from "react-icons/fa";
import { ProfileBlock } from "../../../Components/MyPage/MyPageApplicant/ProfileBlock";
import { CardsComponent } from "../Cards/CardsComponent";
import Loader from "../../../styles/images/Loader/Spinner-1s-200px.svg";
import {
  pageWhrapperObj,
  profileHeadingBlockObj,
  headingIsProfileObj,
  headingNoProfileObj,
  linkCreateProfileObj,
  profileBlockObj,
  spanNoProfileObj,
  cardsHeadingBlockObj,
  headingNoCardsObj,
  spanNoCardsObj,
  cardsBlockObj,
  headingPageBlockObj,
  headingPageObj,
  linkChangeProfileObj,
  headingCreateCardsObj,
  linkCreateCardsObj,
  cardsListBlockObj,
  spanNoCardsListObj,
  headingIsCardsObj,
} from "./Data";

export const MyPageApplicant = () => {
  const { getApiData, loading } = ApiRequests();
  const { userId } = useContext(AuthContext);
  const [userInfo, setUserInfo] = useState(null);
  const [errorStorage, setErrorStorage] = useState(null);

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiData(setUserInfo, `/Applicant/user/${userId}`, setErrorStorage);
  }, [getApiData, userId]);

  useEffect(() => {
    if (errorStorage) {
      alert(errorStorage);
    }
    return () => {
      setErrorStorage(null);
    };
  }, [errorStorage]);

  return (
    <PageWhrapperComponent {...pageWhrapperObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>

      <BlockComponent {...profileHeadingBlockObj}>
        {!loading && !userInfo && (
          <>
            <HeadingComponent {...headingNoProfileObj} />
            <FaArrowDown
              className="heading-my-profile__icon heading-my-profile__icon__active"
              size="30"
            />
          </>
        )}
        {!loading && userInfo && (
          <>
            <HeadingComponent {...headingIsProfileObj} />
            <FaArrowDown className="heading-my-profile__icon " size="30" />
          </>
        )}
      </BlockComponent>

      <BlockComponent {...profileBlockObj}>
        {!loading && !userInfo && (
          <>
            <SpanComponent {...spanNoProfileObj} />
            <LinkComponent {...linkCreateProfileObj} />
          </>
        )}
        {!loading && userInfo && (
          <>
            <ProfileBlock userInfo={userInfo} />
            <LinkComponent
              {...linkChangeProfileObj}
              link={`/change-applicant/${userInfo.id}`}
            />{" "}
          </>
        )}
        {loading && <Loader />}
      </BlockComponent>

      <BlockComponent {...cardsHeadingBlockObj}>
        {!loading && !userInfo && (
          <>
            <HeadingComponent {...headingNoCardsObj} />
            <FaArrowDown className="heading-my-profile__icon " size="30" />
          </>
        )}
        {!loading && userInfo && userInfo.resumes.length === 0 && (
          <>
            <HeadingComponent {...headingCreateCardsObj} />
            <FaArrowDown
              className="heading-my-profile__icon heading-my-profile__icon__active"
              size="30"
            />
          </>
        )}
        {!loading && userInfo && userInfo.resumes.length !== 0 && (
          <>
            <HeadingComponent {...headingIsCardsObj} />
            <FaArrowDown className="heading-my-profile__icon " size="30" />
          </>
        )}
      </BlockComponent>

      <BlockComponent {...cardsBlockObj}>
        {!loading && !userInfo && <SpanComponent {...spanNoCardsObj} />}
        {loading && <Loader />}
        {!loading && userInfo && userInfo.resumes.length === 0 && (
          <>
            <LinkComponent {...linkCreateCardsObj} />
            <BlockComponent {...cardsListBlockObj}>
              <SpanComponent {...spanNoCardsListObj} />
            </BlockComponent>
          </>
        )}
        {!loading && userInfo && userInfo.resumes.length !== 0 && (
          <>
            <LinkComponent {...linkCreateCardsObj} />
            <BlockComponent {...cardsListBlockObj}>
              {userInfo.resumes.map((data) => {
                return (
                  <CardsComponent
                    key={data.id}
                    data={data}
                    urlChange={`/change-resume/`}
                    urlDetail={`/detail-resume/`}
                  />
                );
              })}
            </BlockComponent>
          </>
        )}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
