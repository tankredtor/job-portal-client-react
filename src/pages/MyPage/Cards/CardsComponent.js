import React from "react";
import { BlockComponent } from "../../../Components/BlockComponent/BlockComponent";
import { SpanComponent } from "../../../Components/ReusableComponents/PresenatationComponents/SpanComponent";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import { FaPen } from "react-icons/fa";
import {
  cardsBlockObj,
  spanCardsNameObj,
  spanCardsSalaryObj,
  linkCardsDetailObj,
  spanCardsDateObj,
} from "./Data";
export const CardsComponent = ({ data, urlChange, urlDetail }) => {
  const limitTitle = (title, limit = 20) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split(" ").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join(" ")}...`;
    }
    return title;
  };

  const salaryInfo = () => {
    if (data.salaryFrom && data.salaryTo) {
      return `${data.salaryFrom} - ${data.salaryTo} Руб. `;
    } else if (data.salaryFrom && !data.salaryTo) {
      return `от ${data.salaryFrom} Руб.`;
    } else if (data.salaryTo && !data.salaryFrom) {
      return ` до ${data.salaryTo} Руб.`;
    } else if (data.salaryTo === 0 && data.salaryTo === data.salaryFrom) {
      return "Доход не указан";
    } else return "Доход не указан";
  };

  const dateOfCreation = (date) => {
    let today = new Date(date);
    const dd = String(today.getDate()).padStart(2, "0");
    const mm = String(today.getMonth() + 1).padStart(2, "0");
    const yyyy = today.getFullYear();

    today = dd + "." + mm + "." + yyyy;
    return today;
  };

  return (
    <BlockComponent {...cardsBlockObj}>
      <LinkComponent {...linkCardsDetailObj} link={`${urlDetail}${data.id}`}>
        <SpanComponent {...spanCardsNameObj} body={limitTitle(data.name)} />
        <SpanComponent {...spanCardsSalaryObj} body={salaryInfo()} />
        <SpanComponent
          {...spanCardsDateObj}
          body={`Создано ${dateOfCreation(data.updatedAt)}`}
        />
      </LinkComponent>
      <LinkComponent link={`${urlChange}${data.id}`}>
        <FaPen className="my-card__icon" size="30" />
      </LinkComponent>
    </BlockComponent>
  );
};
