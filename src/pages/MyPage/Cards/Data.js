export const cardsBlockObj = {
  htmlBlock: "my-card",
};

export const spanCardsNameObj = {
  htmlBlock: "my-card__data__name ",
  typography: "heading-block-big",
};

export const spanCardsSalaryObj = {
  htmlBlock: "my-card__data__salary ",
  typography: "heading-sub-information",
};

export const spanCardsDateObj = {
  htmlBlock: "my-card__data__updated-at ",
  typography: "heading-grey",
};

export const linkCardsDetailObj = {
  htmlBlock: "my-card__data",
};
