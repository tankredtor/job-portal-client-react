export default function validateInfo(values, valuesNumber) {
  let errors = {};

  if (values.name.length > 64) {
    errors.name = "Ограничение на ввод 64 символа";
  }

  if (!values.description) {
    errors.description = "Вы не указали описание вакансии";
  }

  if (values.description.length > 1024) {
    errors.description = "Ограничение на ввод 1024 символа";
  }

  if (!values.requirements) {
    errors.requirements = "Вы не указали требования";
  }

  if (values.requirements.length > 1024) {
    errors.requirements = "Ограничение на ввод 1024 символа";
  }

  if (!values.duties) {
    errors.duties = "Вы не указали обязанности";
  }

  if (values.duties.length > 1024) {
    errors.duties = "Ограничение на ввод 1024 символа";
  }

  if (!values.name) {
    errors.name = "Вы не указали название вакансии";
  }

  if (
    valuesNumber.salaryTo &&
    valuesNumber.salaryFrom > valuesNumber.salaryTo
  ) {
    errors.salaryFrom = '"Доход от" не может быть больше "Доход до" ';
  }

  if (valuesNumber.salaryFrom && String(valuesNumber.salaryFrom).length > 10) {
    errors.salaryFrom = "Ограничение на ввод 10 чисел ";
  }
  if (valuesNumber.salaryTo && String(valuesNumber.salaryTo).length > 10) {
    errors.salaryTo = "Ограничение на ввод 10 чисел";
  }

  return errors;
}
