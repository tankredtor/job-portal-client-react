import React, { useState, useCallback, useEffect } from "react";
import { ApiRequests } from "../../api/api.requests";
import { useHistory } from "react-router-dom";
import useForm from "../../hooks/useForm";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import {
  inputObjNameVacancy,
  inputObjSalaryFromVacancy,
  inputObjSalaryToVacancy,
  textAreaObjDescriptionVacancy,
  textAreaObjDutiesVacancy,
  textAreaObjRequirementsVacancy,
  selectObjExperienceVacancy,
  selectObjEmployementVacancy,
  optionObjNoExperience,
  optionObjToThreeYears,
  optionObjToSixYears,
  optionObjOverSixYears,
  optionObjPartialEmployment,
  optionObjFullEmployment,
  FormObjVacancyCreate,
  ButtonObj,
} from "./Data";
import validate from "./validate";

export const CreateVacancyPage = () => {
  const { postApiData } = ApiRequests();
  const history = useHistory();
  const [form, setForm] = useState({
    name: "",
    description: "",
    requirements: "",
    duties: "",
  });
  const [formNumber, setFormNumber] = useState({
    requiredExperience: 0,
    typeOfEmployment: 0,
    salaryFrom: null,
    salaryTo: null,
  });
  const {
    handleSubmit,
    errors,
    isSubmitted,
    changeHandler,
    changeHandlerNumber,
  } = useForm(form, setForm, validate, formNumber, setFormNumber);

  const createButton = useCallback(async () => {
    await postApiData({ ...form, ...formNumber }, "/Vacancy");
    history.push(`/my-page`);
  }, [postApiData, form, formNumber, history]);

  useEffect(() => {
    if (isSubmitted) {
      createButton();
    }
  }, [isSubmitted, createButton]);

  return (
    <div className="create-vacancy-page">
      <FormComponent {...FormObjVacancyCreate}>
        <InputElement
          {...inputObjNameVacancy}
          value={form.name || ""}
          onChange={changeHandler}
        >
          {errors.name && <ErrorElement error={errors.name} />}
        </InputElement>
        <TextAreaElement
          {...textAreaObjDescriptionVacancy}
          value={form.description || ""}
          onChange={changeHandler}
        >
          {errors.description && <ErrorElement error={errors.description} />}
        </TextAreaElement>
        <TextAreaElement
          {...textAreaObjRequirementsVacancy}
          value={form.requirements || ""}
          onChange={changeHandler}
        >
          {errors.requirements && <ErrorElement error={errors.requirements} />}
        </TextAreaElement>
        <TextAreaElement
          {...textAreaObjDutiesVacancy}
          value={form.duties || ""}
          onChange={changeHandler}
        >
          {errors.duties && <ErrorElement error={errors.duties} />}
        </TextAreaElement>
        <SelectElement
          {...selectObjExperienceVacancy}
          value={formNumber.requiredExperience}
          onChange={changeHandlerNumber}
        >
          <OptionElement {...optionObjNoExperience} />
          <OptionElement {...optionObjToThreeYears} />
          <OptionElement {...optionObjToSixYears} />
          <OptionElement {...optionObjOverSixYears} />
        </SelectElement>

        <SelectElement
          {...selectObjEmployementVacancy}
          value={formNumber.typeOfEmployment}
          onChange={changeHandlerNumber}
        >
          <OptionElement {...optionObjPartialEmployment} />
          <OptionElement {...optionObjFullEmployment} />
        </SelectElement>

        <InputElement
          {...inputObjSalaryFromVacancy}
          value={formNumber.salaryFrom || ""}
          onChange={changeHandlerNumber}
        >
          {errors.salaryFrom && <ErrorElement error={errors.salaryFrom} />}
        </InputElement>
        <InputElement
          {...inputObjSalaryToVacancy}
          value={formNumber.salaryTo || ""}
          onChange={changeHandlerNumber}
        >
          {errors.salaryTo && <ErrorElement error={errors.salaryTo} />}
        </InputElement>
        <ButtonStandartComponent {...ButtonObj} onClick={handleSubmit} />
      </FormComponent>
    </div>
  );
};
