export default function validateInfo(values) {
  let errors = {};

  const isValidUrl = (url) => {
    try {
      new URL(url);
    } catch (e) {
      console.error(e);
      return false;
    }
    return true;
  };

  if (!values.infoUrl) {
    errors.infoUrl = "Вы не указали ссылку на ваш сайт";
  } else if (values.infoUrl.length > 64) {
    errors.infoUrl = "Ограничение на ввод 64 символа";
  } else if (values.infoUrl && !isValidUrl(values.infoUrl)) {
    errors.infoUrl =
      "Вы должны указать действительный адрес сайта в формате https://... или http://...";
  }

  if (!values.contactEmail) {
    errors.contactEmail = "Вы не указали E-mail";
  } else if (!/\S+@\S+\.\S+/.test(values.contactEmail)) {
    errors.contactEmail = "Введен некорректный E-mail";
  } else if (values.contactEmail.length > 64) {
    errors.contactEmail = "Ограничение на ввод 64 символа";
  }

  if (values.contactPhone && values.contactPhone.length < 11) {
    errors.contactPhone = "Номер не может быть меньше 11 символов";
  } else if (values.contactPhone.length > 11) {
    errors.contactPhone = "Номер не может быть больше 11 символов";
  }

  if (!values.description) {
    errors.description = "Вы не описали деятельность организации";
  }

  if (values.description.length > 1024) {
    errors.description = "Ограничение на ввод 1024 символа";
  }

  if (!values.name) {
    errors.name = "Вы не указали название организации";
  } else if (values.name.length > 64) {
    errors.name = "Номер не может быть больше 11 символов";
  }

  return errors;
}
