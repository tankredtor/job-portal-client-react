import React, { useState, useEffect, useCallback } from "react";
import useForm from "../../hooks/useForm";
import { useHistory } from "react-router-dom";
import { ApiRequests } from "../../api/api.requests";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import {
  formObjEmployerCreate,
  buttonObj,
  textAreaObjNameEmployer,
  textAreaObjDescriptionEmployer,
  inputObjInfoUrlEmployer,
  inputObjContactPhoneEmployer,
  inputObjContactEmailEmployer,
} from "./Data";
import validate from "./validate";

export const CreateEmployerPage = () => {
  const { postApiData } = ApiRequests();
  const history = useHistory();
  const [form, setForm] = useState({
    name: "",
    description: "",
    infoUrl: "",
    contactEmail: "",
    contactPhone: "",
  });
  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate
  );

  const createButton = useCallback(async () => {
    await postApiData({ ...form }, "/Employer");
    history.push(`/my-page`);
  }, [form, postApiData, history]);

  useEffect(() => {
    if (isSubmitted) {
      createButton();
    }
  }, [isSubmitted, createButton]);

  return (
    <div className="create-employer-page">
      <FormComponent {...formObjEmployerCreate}>
        <TextAreaElement
          {...textAreaObjNameEmployer}
          value={form.name}
          onChange={changeHandler}
        >
          {errors.name && <ErrorElement error={errors.name} />}
        </TextAreaElement>
        <TextAreaElement
          {...textAreaObjDescriptionEmployer}
          value={form.description}
          onChange={changeHandler}
        >
          {errors.description && <ErrorElement error={errors.description} />}
        </TextAreaElement>
        <InputElement
          {...inputObjInfoUrlEmployer}
          value={form.infoUrl}
          onChange={changeHandler}
        >
          {errors.infoUrl && <ErrorElement error={errors.infoUrl} />}
        </InputElement>

        <InputElement
          {...inputObjContactPhoneEmployer}
          value={form.contactPhone}
          onChange={changeHandler}
        >
          {errors.contactPhone && <ErrorElement error={errors.contactPhone} />}
        </InputElement>
        <InputElement
          {...inputObjContactEmailEmployer}
          value={form.contactEmail}
          onChange={changeHandler}
        >
          {errors.contactEmail && <ErrorElement error={errors.contactEmail} />}
        </InputElement>
        <ButtonStandartComponent {...buttonObj} onClick={handleSubmit} />
      </FormComponent>
    </div>
  );
};
