import React, { useState, useEffect, useCallback } from "react";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { ApiRequests } from "../../api/api.requests";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import useForm from "../../hooks/useForm";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import { AiFillDelete } from "react-icons/ai";
import { ConfirmBannerComponent } from "../../Components/Banner/ConfirmBannerComponent/ConfirmBannerComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import validate from "./validate";
import {
  buttonObjChange,
  formObjApplicantChange,
  inputObjCitezenshipApplicant,
  inputObjCityApplicant,
  inputObjEmailApplicant,
  inputObjFacebookLinkApplicant,
  inputObjFirstNameApplicant,
  inputObjInstagramLinkApplicant,
  inputObjLastNameApplicant,
  inputObjPhoneApplicant,
  inputObjVkLinkApplicant,
  selectObjSortingFieldNameApplicant,
  optionObjFemale,
  optionObjMale,
  confirmObj,
} from "./Data";

export const ChangeApplicantPage = () => {
  const { getApiData, putApiData, deleteApiData } = ApiRequests();
  const history = useHistory();
  const applicantId = useParams().id;
  const [form, setForm] = useState({
    id: null,
    firstName: null,
    lastName: null,
    city: null,
    instagramLink: null,
    facebookLink: null,
    vkLink: null,
    gender: null,
    citizenship: null,
    contactPhone: null,
    contactEmail: null,
  });
  const [confirmShow, setConfirmShow] = useState(false);
  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate
  );

  const changeButton = useCallback(async () => {
    await putApiData(
      {
        ...form,
      },
      `/Applicant`
    );
    history.push(`/my-page`);
  }, [putApiData, form, history]);

  const onConfirmShow = (e) => {
    e.preventDefault();
    setConfirmShow(!confirmShow);
  };

  const deleteButton = async (event) => {
    event.preventDefault();
    await deleteApiData(`/Applicant/${applicantId}`);
    history.push(`/my-page`);
  };

  useEffect(() => {
    getApiData(setForm, `/Applicant/${applicantId}`);
  }, [getApiData, applicantId]);

  useEffect(() => {
    if (isSubmitted) {
      changeButton();
    }
  }, [isSubmitted, changeButton]);

  return (
    <div className="create-applicant-page">
      <FormComponent {...formObjApplicantChange}>
        <BlockComponent {...confirmObj}>
          {!confirmShow && (
            <AiFillDelete
              onClick={onConfirmShow}
              className="delete-icon"
              size="30"
            />
          )}
          {confirmShow && (
            <ConfirmBannerComponent
              confirm={deleteButton}
              refusal={onConfirmShow}
            />
          )}
        </BlockComponent>
        <InputElement
          {...inputObjFirstNameApplicant}
          value={form.firstName}
          onChange={changeHandler}
        >
          {errors.firstName && <ErrorElement error={errors.firstName} />}
        </InputElement>
        <InputElement
          {...inputObjLastNameApplicant}
          value={form.lastName}
          onChange={changeHandler}
        >
          {errors.lastName && <ErrorElement error={errors.lastName} />}
        </InputElement>

        <InputElement
          {...inputObjCityApplicant}
          value={form.city}
          onChange={changeHandler}
        >
          {errors.city && <ErrorElement error={errors.city} />}
        </InputElement>
        <InputElement
          {...inputObjInstagramLinkApplicant}
          value={form.instagramLink}
          onChange={changeHandler}
        >
          {errors.instagramLink && (
            <ErrorElement error={errors.instagramLink} />
          )}
        </InputElement>
        <InputElement
          {...inputObjFacebookLinkApplicant}
          value={form.facebookLink}
          onChange={changeHandler}
        >
          {errors.facebookLink && <ErrorElement error={errors.facebookLink} />}
        </InputElement>
        <InputElement
          {...inputObjVkLinkApplicant}
          value={form.vkLink}
          onChange={changeHandler}
        >
          {errors.vkLink && <ErrorElement error={errors.vkLink} />}
        </InputElement>
        <SelectElement
          {...selectObjSortingFieldNameApplicant}
          value={form.gender}
          onChange={changeHandler}
        >
          <OptionElement {...optionObjMale} />
          <OptionElement {...optionObjFemale} />
        </SelectElement>

        <InputElement
          {...inputObjCitezenshipApplicant}
          value={form.citizenship}
          onChange={changeHandler}
        >
          {errors.citizenship && <ErrorElement error={errors.citizenship} />}
        </InputElement>
        <InputElement
          {...inputObjPhoneApplicant}
          value={form.contactPhone}
          onChange={changeHandler}
        >
          {errors.contactPhone && <ErrorElement error={errors.contactPhone} />}
        </InputElement>
        <InputElement
          {...inputObjEmailApplicant}
          value={form.contactEmail}
          onChange={changeHandler}
        >
          {errors.contactEmail && <ErrorElement error={errors.contactEmail} />}
        </InputElement>
        <ButtonStandartComponent {...buttonObjChange} onClick={handleSubmit} />
      </FormComponent>
    </div>
  );
};
