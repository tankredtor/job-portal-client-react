export default function validateInfo(values) {
  let errors = {};

  if (!values.firstName) {
    errors.firstName = "Вы не указали Имя";
  }

  if (values.firstName.length > 64) {
    errors.firstName = "Ограничение на ввод 64 символа";
  }

  if (!values.lastName) {
    errors.lastName = "Вы не указали Фамилию";
  }

  if (values.lastName.length > 64) {
    errors.lastName = "Ограничение на ввод 64 символа";
  }

  if (!values.city) {
    errors.city = "Вы не указали город проживания";
  }
  if (values.city.length > 64) {
    errors.city = "Ограничение на ввод 64 символа";
  }
  if (!values.citizenship) {
    errors.citizenship = "Вы не указали граждаство";
  }
  if (values.citizenship.length > 64) {
    errors.citizenship = "Ограничение на ввод 64 символа";
  }
  if (values.contactPhone && values.contactPhone.length < 11) {
    errors.contactPhone = "Телефон должен состоять из 11 цифр";
  }
  if (values.contactPhone.length > 11) {
    errors.contactPhone = "Телефон должен состоять из 11 цифр";
  }

  if (
    values.instagramLink &&
    !/(https?)?:?(www)?instagram\.com/.test(values.instagramLink)
  ) {
    errors.instagramLink =
      "Вы должны ввести существующую ссылку на instagram в формате https://instagram.com/...";
  }

  if (values.instagramLink.length > 64) {
    errors.instagramLink = "Ограничение на ввод 64 символа";
  }

  if (values.vkLink && !/(https?)?:?(www)?vk\.com/.test(values.vkLink)) {
    errors.vkLink =
      "Вы должны ввести существующую ссылку на страницу в vk в формате https://vk.com/...";
  }
  if (values.vkLink.length > 64) {
    errors.vkLink = "Ограничение на ввод 64 символа";
  }

  if (
    values.facebookLink &&
    !/(https?)?:?(www)?facebook\.com/.test(values.facebookLink)
  ) {
    errors.facebookLink =
      "Вы должны ввести существующую ссылку на страницу в facebook в формате https://facebook.com/...";
  }
  if (values.facebookLink.length > 64) {
    errors.facebookLink = "Ограничение на ввод 64 символа";
  }

  if (!values.contactEmail) {
    errors.contactEmail = "Вы не указали E-mail";
  } else if (!/\S+@\S+\.\S+/.test(values.contactEmail)) {
    errors.contactEmail = "Введен некорректный E-mail";
  }
  if (values.contactEmail.length > 64) {
    errors.contactEmail = "Ограничение на ввод 64 символа";
  }

  return errors;
}
