import React, { useCallback, useEffect, useState } from "react";
import { ApiRequests } from "../../api/api.requests";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement.js";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import useForm from "../../hooks/useForm";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import { AiFillDelete } from "react-icons/ai";
import { ConfirmBannerComponent } from "../../Components/Banner/ConfirmBannerComponent/ConfirmBannerComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import validate from "./validate";
import {
  formObjResumeChange,
  buttonObjChange,
  inputObjNameResume,
  inputObjSalaryFromResume,
  inputObjSalaryToResume,
  textAreaObjAboutMeResume,
  selectObjEducationStatusResume,
  optionObjAD,
  optionObjBachelorNow,
  optionObjIsBachelorNoMaster,
  optionObjIsMaster,
  optionObjIsSpecialist,
  optionObjMasterNow,
  optionObjPG,
  optionObjSpecialistNow,
  selectObjExperienceResume,
  optionObjNoExperience,
  optionObjOverSixYears,
  optionObjToThreeYears,
  optionObjToSixYears,
  confirmObj,
} from "./Data";

export const ChangeResumePage = () => {
  const { deleteApiData, putApiData, getApiData, loading } = ApiRequests();
  const resumeId = useParams().id;
  const history = useHistory();
  const [form, setForm] = useState(null);
  const [formNumber, setFormNumber] = useState(null);
  const [formString, setFormString] = useState(null);
  const [confirmShow, setConfirmShow] = useState(false);

  const parseInteger = useCallback(() => {
    if (form) {
      setFormNumber({
        experience: parseInt(form.experience, 10),
        salaryFrom: parseInt(form.salaryFrom, 10),
        salaryTo: parseInt(form.salaryTo, 10),
        educationStatus: parseInt(form.educationStatus, 10),
        id: parseInt(form.id, 10),
      });
      setFormString({
        name: form.name,
        aboutMe: form.aboutMe,
      });
    }
  }, [form]);

  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate,
    formNumber,
    setFormNumber
  );
  console.log(formNumber);
  const changeButton = useCallback(async () => {
    await putApiData(
      {
        ...formString,
        ...formNumber,
      },
      `/Resume`
    );
    history.push(`/detail-resume/${resumeId}`);
  }, [putApiData, formString, formNumber, history, resumeId]);

  const deleteButton = async (event) => {
    event.preventDefault();
    await deleteApiData(`/Resume/${resumeId}`);
    history.push(`/my-page`);
  };

  const onConfirmShow = (e) => {
    e.preventDefault();
    setConfirmShow(!confirmShow);
  };

  useEffect(() => {
    getApiData(setForm, `/Resume/${resumeId}`);
  }, [getApiData, resumeId]);

  useEffect(() => {
    parseInteger();
  }, [parseInteger]);

  useEffect(() => {
    if (isSubmitted) {
      changeButton();
    }
  }, [isSubmitted, changeButton]);

  return (
    <>
      {!loading && form && (
        <div className="change-resume-page">
          <FormComponent {...formObjResumeChange}>
            <BlockComponent {...confirmObj}>
              {!confirmShow && (
                <AiFillDelete
                  onClick={onConfirmShow}
                  className="delete-icon"
                  size="30"
                />
              )}
              {confirmShow && (
                <ConfirmBannerComponent
                  confirm={deleteButton}
                  refusal={onConfirmShow}
                />
              )}
            </BlockComponent>
            <InputElement
              {...inputObjNameResume}
              value={form.name || ""}
              onChange={changeHandler}
            >
              {" "}
              {errors.name && <ErrorElement error={errors.name} />}
            </InputElement>

            <TextAreaElement
              {...textAreaObjAboutMeResume}
              value={form.aboutMe || ""}
              onChange={changeHandler}
            >
              {errors.aboutMe && <ErrorElement error={errors.aboutMe} />}
            </TextAreaElement>

            <SelectElement
              {...selectObjEducationStatusResume}
              value={form.educationStatus}
              onChange={changeHandler}
            >
              <OptionElement {...optionObjBachelorNow} />
              <OptionElement {...optionObjIsBachelorNoMaster} />
              <OptionElement {...optionObjSpecialistNow} />
              <OptionElement {...optionObjIsSpecialist} />
              <OptionElement {...optionObjMasterNow} />
              <OptionElement {...optionObjIsMaster} />
              <OptionElement {...optionObjPG} />
              <OptionElement {...optionObjAD} />
            </SelectElement>

            <SelectElement
              {...selectObjExperienceResume}
              value={form.experience}
              onChange={changeHandler}
            >
              <OptionElement {...optionObjNoExperience} />
              <OptionElement {...optionObjToThreeYears} />
              <OptionElement {...optionObjToSixYears} />
              <OptionElement {...optionObjOverSixYears} />
            </SelectElement>

            <InputElement
              {...inputObjSalaryFromResume}
              value={form.salaryFrom || ""}
              onChange={changeHandler}
            >
              {errors.salaryFrom && <ErrorElement error={errors.salaryFrom} />}
            </InputElement>

            <InputElement
              {...inputObjSalaryToResume}
              value={form.salaryTo || ""}
              onChange={changeHandler}
            >
              {errors.salaryTo && <ErrorElement error={errors.salaryTo} />}{" "}
            </InputElement>
            <ButtonStandartComponent
              {...buttonObjChange}
              onClick={handleSubmit}
            />
          </FormComponent>
        </div>
      )}
    </>
  );
};
