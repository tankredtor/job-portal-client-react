export const pageWrapperObj = {
  htmlName: "privacy",
};

export const linkBackObj = {
  link: "/",
  htmlBlock: "privacy-page__arrow-back",
};

export const dimondsBlockObj = {
  htmlBlock: "diamonds-block",
};

export const headingBlockObj = {
  htmlBlock: "heading-page",
};

export const headingObj = {
  htmlBlock: "heading-privacy",
  typography: "heading-main-information",
  body: "Политика обработки персональных данных",
};

export const textBlockObj = {
  htmlBlock: "privacy-text-block",
};

export const spanBold1Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "1. Общие положения",
};
export const spanBold2Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "2. Основные понятия, используемые в Политике",
};
export const spanBold3Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body:
    "3. Оператор может обрабатывать следующие персональные данные Пользователя",
};
export const spanBold4Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "4. Цели обработки персональных данных",
};
export const spanBold5Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "5. Правовые основания обработки персональных данных",
};
export const spanBold6Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body:
    "6. Порядок сбора, хранения, передачи и других видов обработки персональных данных",
};
export const spanBold7Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "7. Трансграничная передача персональных данных",
};
export const spanBold8Obj = {
  htmlBlock: "privacy-text-block__span-bold",
  typography: "heading-block-medium-bold",
  body: "8. Заключительные положения",
};

export const spanMain1Obj = {
  htmlBlock: "privacy-text-block__span",
  typography: "heading-sub-information",
};
