export const formObjVacancyChange = {
  headingPrimary: " Введите данные для изменения вакансии",
  headingSecondary:
    'Введенные Вами данные будут доступны для просмотра студентам и выпускникам МГЮА (Университета имени О.Е. Кутафина) в разделе "Вакансии" . Убедитесь, что в Вашем профиле работодателя указаны контактные данные, чтобы потенциальные работники могли связаться с Вами и направить Резюме.',
};

export const buttonObjChange = {
  description: "Изменить",
  htmlName: "change-vacancy",
};

export const confirmObj = {
  htmlBlock: "confirm-block",
};

export const buttonObjDelete = {
  description: "Удалить",
  htmlName: "delete-vacancy",
};

export const inputObjNameVacancy = {
  heading: "Название вакансии",
  type: "text",
  placeholder: "Название",
  name: "name",
  size: "small",
};

export const inputObjSalaryFromVacancy = {
  heading: "Зарплату от",
  type: "number",
  placeholder: "0",
  name: "salaryFrom",
  size: "small",
};

export const inputObjSalaryToVacancy = {
  heading: "Зарплату до",
  type: "number",
  placeholder: "0",
  name: "salaryTo",
  size: "small",
};

export const textAreaObjDescriptionVacancy = {
  heading: "Описание вакансии",
  type: "text",
  placeholder: "Расскажите о вакансии",
  name: "description",
  size: "big",
};

export const textAreaObjRequirementsVacancy = {
  heading: "Требования",
  type: "text",
  placeholder: "Что вы ждете от соискателя?",
  name: "requirements",
  size: "big",
};

export const textAreaObjDutiesVacancy = {
  heading: "Обязанности",
  type: "text",
  placeholder: "Что должен будет делать работник?",
  name: "duties",
  size: "big",
};

export const selectObjExperienceVacancy = {
  heading: "Требуемый опыт работы",
  type: "number",
  name: "requiredExperience",
};

export const selectObjEmployementVacancy = {
  heading: "Форма занятости",
  type: "number",
  name: "typeOfEmployment",
};

export const optionObjNoExperience = {
  type: "number",
  value: 0,
  description: "Без опыта",
};

export const optionObjToThreeYears = {
  type: "number",
  value: 1,
  description: "От 1 года до 3 лет",
};

export const optionObjToSixYears = {
  type: "number",
  value: 2,
  description: "От 3 до 6 лет",
};

export const optionObjOverSixYears = {
  type: "number",
  value: 3,
  description: "Более 6 лет",
};

export const optionObjPartialEmployment = {
  type: "number",
  value: 0,
  description: "Частичная занятость",
};

export const optionObjFullEmployment = {
  type: "number",
  value: 1,
  description: "Полная занятость",
};

export const optionObjNoSelect = {
  type: "number",
  value: null,
  description: "Не выбрано",
};
