import React, { useCallback, useEffect, useState } from "react";
import { ApiRequests } from "../../api/api.requests";
import { useParams } from "react-router-dom";
import { useHistory } from "react-router-dom";
import useForm from "../../hooks/useForm";
import { TextAreaElement } from "../../Components/CreateForm/TextAreaElement.js";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import { AiFillDelete } from "react-icons/ai";
import { ConfirmBannerComponent } from "../../Components/Banner/ConfirmBannerComponent/ConfirmBannerComponent";
import {
  inputObjNameVacancy,
  inputObjSalaryFromVacancy,
  inputObjSalaryToVacancy,
  textAreaObjDescriptionVacancy,
  textAreaObjDutiesVacancy,
  textAreaObjRequirementsVacancy,
  selectObjExperienceVacancy,
  selectObjEmployementVacancy,
  optionObjNoExperience,
  optionObjToThreeYears,
  optionObjToSixYears,
  optionObjOverSixYears,
  optionObjPartialEmployment,
  optionObjFullEmployment,
  buttonObjChange,
  formObjVacancyChange,
  confirmObj,
} from "./Data";
import validate from "./validate";

export const ChangeVacancyPage = () => {
  const { deleteApiData, putApiData, getApiData, loading } = ApiRequests();
  const [form, setForm] = useState(null);
  const vacancyId = useParams().id;
  const history = useHistory();
  const [formNumber, setFormNumber] = useState(null);
  const [formString, setFormString] = useState(null);
  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate,
    formNumber,
    setFormNumber
  );
  const [confirmShow, setConfirmShow] = useState(false);
  const parseInteger = useCallback(() => {
    if (form) {
      setFormNumber({
        requiredExperience: parseInt(form.requiredExperience, 10),
        salaryFrom: parseInt(form.salaryFrom, 10),
        salaryTo: parseInt(form.salaryTo, 10),
        typeOfEmployment: parseInt(form.typeOfEmployment, 10),
        id: parseInt(form.id, 10),
      });
      setFormString({
        name: form.name,
        description: form.description,
        requirements: form.requirements,
        duties: form.duties,
        createdAt: form.createdAt,
      });
    }
  }, [form]);

  const changeButton = useCallback(async () => {
    await putApiData(
      {
        ...formNumber,
        ...formString,
      },
      `/Vacancy`
    );
    history.push(`/detail-vacancy/${vacancyId}`);
  }, [putApiData, history, formNumber, formString, vacancyId]);

  const deleteButton = async (event) => {
    event.preventDefault();
    await deleteApiData(`/Vacancy/${vacancyId}`);
    history.push(`/my-page`);
  };

  const onConfirmShow = (e) => {
    e.preventDefault();
    setConfirmShow(!confirmShow);
  };

  useEffect(() => {
    getApiData(setForm, `/Vacancy/${vacancyId}`);
  }, [getApiData, vacancyId]);

  useEffect(() => {
    parseInteger();
  }, [parseInteger]);

  useEffect(() => {
    if (isSubmitted) {
      changeButton();
    }
  }, [isSubmitted, changeButton]);

  return (
    <>
      {!loading && formNumber && (
        <div className="change-resume-page">
          <FormComponent {...formObjVacancyChange}>
            <BlockComponent {...confirmObj}>
              {!confirmShow && (
                <AiFillDelete
                  onClick={onConfirmShow}
                  className="delete-icon"
                  size="30"
                />
              )}
              {confirmShow && (
                <ConfirmBannerComponent
                  confirm={deleteButton}
                  refusal={onConfirmShow}
                />
              )}
            </BlockComponent>
            <InputElement
              {...inputObjNameVacancy}
              value={form.name || ""}
              onChange={changeHandler}
            >
              {errors.name && <ErrorElement error={errors.name} />}
            </InputElement>
            <TextAreaElement
              {...textAreaObjDescriptionVacancy}
              value={form.description || ""}
              onChange={changeHandler}
            >
              {errors.description && (
                <ErrorElement error={errors.description} />
              )}
            </TextAreaElement>
            <TextAreaElement
              {...textAreaObjRequirementsVacancy}
              value={form.requirements || ""}
              onChange={changeHandler}
            >
              {errors.requirements && (
                <ErrorElement error={errors.requirements} />
              )}
            </TextAreaElement>
            <TextAreaElement
              {...textAreaObjDutiesVacancy}
              value={form.duties || ""}
              onChange={changeHandler}
            >
              {errors.duties && <ErrorElement error={errors.duties} />}
            </TextAreaElement>
            <SelectElement
              {...selectObjExperienceVacancy}
              value={form.requiredExperience}
              onChange={changeHandler}
            >
              <OptionElement {...optionObjNoExperience} />
              <OptionElement {...optionObjToThreeYears} />
              <OptionElement {...optionObjToSixYears} />
              <OptionElement {...optionObjOverSixYears} />
            </SelectElement>

            <SelectElement
              {...selectObjEmployementVacancy}
              value={form.typeOfEmployment}
              onChange={changeHandler}
            >
              <OptionElement {...optionObjPartialEmployment} />
              <OptionElement {...optionObjFullEmployment} />
            </SelectElement>

            <InputElement
              {...inputObjSalaryFromVacancy}
              value={form.salaryFrom || ""}
              onChange={changeHandler}
            >
              {errors.salaryFrom && <ErrorElement error={errors.salaryFrom} />}
            </InputElement>
            <InputElement
              {...inputObjSalaryToVacancy}
              value={form.salaryTo || ""}
              onChange={changeHandler}
            >
              {errors.salaryTo && <ErrorElement error={errors.salaryTo} />}
            </InputElement>
            <ButtonStandartComponent
              {...buttonObjChange}
              onClick={handleSubmit}
            />
          </FormComponent>
        </div>
      )}
    </>
  );
};
