export const formObjApplicantCreate = {
  headingPrimary: " Введите данные для создания профиля соискателя",
  headingSecondary:
    'Введенные Вами данные будут доступны для просмотра Работодателям при переходе по ссылке вашего Резюме (которое вы еще не создали) в разделе "Резюме" . Убедитесь, что указываете актуальные данные, чтобы работодатель мог связаться с Вами и направить Резюме.',
};

export const buttonObj = {
  description: "Создать профиль",
  htmlName: "create-vacancy",
};

export const inputObjFirstNameApplicant = {
  heading: "Имя",
  type: "text",
  placeholder: "Иван",
  name: "firstName",
  size: "small",
};

export const inputObjLastNameApplicant = {
  heading: "Фамилия",
  type: "text",
  placeholder: "Иванов",
  name: "lastName",
  size: "small",
};

export const inputObjCityApplicant = {
  heading: "Город проживания",
  type: "text",
  placeholder: "Москва",
  name: "city",
  size: "small",
};

export const inputObjInstagramLinkApplicant = {
  heading: "Ссылка на Instagram",
  type: "text",
  placeholder: "@msal_alumni",
  name: "instagramLink",
  size: "small",
};

export const inputObjFacebookLinkApplicant = {
  heading: "Ссылка на Facebook",
  type: "text",
  placeholder: "URL страницы на Facebook",
  name: "facebookLink",
  size: "small",
};

export const inputObjVkLinkApplicant = {
  heading: "Ссылка на VK",
  type: "text",
  placeholder: "URL страницы в VK",
  name: "vkLink",
  size: "small",
};

export const inputObjCitezenshipApplicant = {
  heading: "Гражданство",
  type: "text",
  placeholder: "Российская Федерация",
  name: "citizenship",
  size: "small",
};

export const inputObjPhoneApplicant = {
  heading: "Телефон для связи",
  type: "text",
  placeholder: "Телефон",
  name: "contactPhone",
  size: "small",
};

export const inputObjEmailApplicant = {
  heading: "E-mail для связи",
  type: "text",
  placeholder: "user@email.ru",
  name: "contactEmail",
  size: "small",
};

export const selectObjSortingFieldNameApplicant = {
  heading: "Пол",
  type: "text",
  name: "gender",
};

export const optionObjMale = {
  type: "text",
  value: "man",
  description: "Мужской",
};

export const optionObjFemale = {
  type: "text",
  value: "woman",
  description: "Женский",
};
