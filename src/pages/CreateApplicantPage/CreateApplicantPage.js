import React, { useCallback, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { ApiRequests } from "../../api/api.requests";
import { FormComponent } from "../../Components/CreateForm/FormComponent";
import { InputElement } from "../../Components/CreateForm/InputElement";
import { SelectElement } from "../../Components/CreateForm/SelectElement";
import { OptionElement } from "../../Components/CreateForm/OptionElement";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import useForm from "../../hooks/useForm";
import { ErrorElement } from "../../Components/CreateForm/ErrorElement";
import validate from "./validate";
import {
  buttonObj,
  formObjApplicantCreate,
  inputObjCitezenshipApplicant,
  inputObjCityApplicant,
  inputObjEmailApplicant,
  inputObjFacebookLinkApplicant,
  inputObjFirstNameApplicant,
  inputObjInstagramLinkApplicant,
  inputObjLastNameApplicant,
  inputObjPhoneApplicant,
  inputObjVkLinkApplicant,
  selectObjSortingFieldNameApplicant,
  optionObjFemale,
  optionObjMale,
} from "./Data";

export const CreateApplicantPage = () => {
  const { postApiData } = ApiRequests();
  const history = useHistory();
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    city: "",
    instagramLink: "",
    facebookLink: "",
    vkLink: "",
    citizenship: "",
    contactPhone: "",
    contactEmail: "",
    gender: "man",
  });
  const { handleSubmit, errors, isSubmitted, changeHandler } = useForm(
    form,
    setForm,
    validate
  );

  const createButton = useCallback(async () => {
    await postApiData({ ...form }, "/Applicant");
    history.push(`/my-page`);
  }, [postApiData, history, form]);

  useEffect(() => {
    if (isSubmitted) {
      createButton();
    }
  }, [isSubmitted, createButton]);

  return (
    <div className="create-applicant-page">
      <FormComponent {...formObjApplicantCreate}>
        <InputElement
          {...inputObjFirstNameApplicant}
          value={form.firstName}
          onChange={changeHandler}
        >
          {errors.firstName && <ErrorElement error={errors.firstName} />}
        </InputElement>
        <InputElement
          {...inputObjLastNameApplicant}
          value={form.lastName}
          onChange={changeHandler}
        >
          {errors.lastName && <ErrorElement error={errors.lastName} />}
        </InputElement>

        <InputElement
          {...inputObjCityApplicant}
          value={form.city}
          onChange={changeHandler}
        >
          {errors.city && <ErrorElement error={errors.city} />}
        </InputElement>
        <InputElement
          {...inputObjInstagramLinkApplicant}
          value={form.instagramLink}
          onChange={changeHandler}
        >
          {errors.instagramLink && (
            <ErrorElement error={errors.instagramLink} />
          )}
        </InputElement>
        <InputElement
          {...inputObjFacebookLinkApplicant}
          value={form.facebookLink}
          onChange={changeHandler}
        >
          {errors.facebookLink && <ErrorElement error={errors.facebookLink} />}
        </InputElement>
        <InputElement
          {...inputObjVkLinkApplicant}
          value={form.vkLink}
          onChange={changeHandler}
        >
          {errors.vkLink && <ErrorElement error={errors.vkLink} />}
        </InputElement>
        <SelectElement
          {...selectObjSortingFieldNameApplicant}
          value={form.gender}
          onChange={changeHandler}
        >
          <OptionElement {...optionObjMale} />
          <OptionElement {...optionObjFemale} />
        </SelectElement>

        <InputElement
          {...inputObjCitezenshipApplicant}
          value={form.citizenship}
          onChange={changeHandler}
        >
          {errors.citizenship && <ErrorElement error={errors.citizenship} />}
        </InputElement>
        <InputElement
          {...inputObjPhoneApplicant}
          value={form.contactPhone}
          onChange={changeHandler}
        >
          {errors.contactPhone && <ErrorElement error={errors.contactPhone} />}
        </InputElement>
        <InputElement
          {...inputObjEmailApplicant}
          value={form.contactEmail}
          onChange={changeHandler}
        >
          {errors.contactEmail && <ErrorElement error={errors.contactEmail} />}
        </InputElement>
        <ButtonStandartComponent {...buttonObj} onClick={handleSubmit} />
      </FormComponent>
    </div>
  );
};
