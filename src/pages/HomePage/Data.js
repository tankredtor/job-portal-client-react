const htmlName = "home";

export const pageObjHome = {
  htmlName,
};
export const LogoObjHome = {
  htmlBlock: "home-page",
  htmlTag: "__logo-component",
};
export const logoAlumniObjHome = {
  htmlName,
  src:
    "https://thumb.tildacdn.com/tild6365-6664-4163-b333-356532363537/-/resize/280x/-/format/webp/___1.png",
};

export const logoProjectObjHome = {
  htmlName,
};

export const usersObjHome = {
  htmlBlock: "block-users",
};

export const validationBlockObjHome = {
  htmlBlock: "home-page__validation",
};
export const validationButtonsObjHome = {
  htmlBlock: "home-page__validation-buttons",
};

export const whrapperInformationObjHome = {
  htmlBlock: "home-page",
  htmlTag: "__whrapper-for-information",
};
export const ButtonEnterObjHome = {
  htmlName: "enter-validation",
  description: "ВХОД",
};
export const ButtonRegistrationObjHome = {
  htmlName: "registration-validation",
  description: "РЕГИСТРАЦИЯ",
};

export const applicantBlockObjHome = {
  htmlBlock: "block-users",
  htmlTag: "__applicant",
};

export const employerBlockObjHome = {
  htmlBlock: "block-users",
  htmlTag: "__employer",
};

export const applicantCardObj = {
  htmlTagImg: "__image",
  htmlTagList: "__list",
  htmlTagHeading: "__heading",
  htmlBlock: "user-card-home",
  htmlElement: "__text",
  htmlElementPointer: "__pointer",
  headingText: "Cтудентам/Выпускникам",
  imgAlt: "imgApplicant",
  elementText1: "Вакансии от профильных работодателей ",
  elementText2: "Стажировки ",
  elementText3: "Прохождение практики",
  elementPointer1: "•",
  elementPointer2: "•",
  elementPointer3: "•",
};

export const employerCardObj = {
  htmlTagImg: "__image",
  htmlTagList: "__list",
  htmlTagHeading: "__heading",
  htmlBlock: "user-card-home",
  htmlElement: "__text",
  htmlElementPointer: "__pointer",
  headingText: "Работодателям",
  imgAlt: "imgEmployer",
  elementText1: "Резюме студентов и выпускников ведущего юридического вуза",
  elementText2: "Партнерство с Содружеством выпускников МГЮА",
  elementPointer1: "•",
  elementPointer2: "•",
  elementPointer3: "",
};
