export default function errorsforBanner(errorMessage) {
  let errors = {};

  if (errorMessage.includes("User already exists")) {
    errors.heading = "Ошибка в создании профиля";
    errors.body = "Пользователь с таким Логином существует";
  }

  if (
    errorMessage.includes(
      "The field Password must be a string or array type with a maximum length of '64'."
    )
  ) {
    errors.heading = "Ошибка при создании пароля";
    errors.body =
      "При создании пароля указаны неверные символы или превышено значение символов";
  }
  if (
    errorMessage.includes("Ошибка в соединении с сервером, попробуйте позднее")
  ) {
    errors.heading = "Ошибка";
    errors.body = "Ошибка в соединении с сервером, попробуйте позднее";
  }

  return errors;
}
