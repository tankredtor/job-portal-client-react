const htmlTag = "registration";
const typography = "registration";

export const FormObj = {
  htmlTag,
};

export const radioApplicantObj = {
  value: "Applicant",
  name: "roleName",
  label: "Ищу работу",
  typography,
};
export const radioEmployerObj = {
  value: "Employer",
  name: "roleName",
  label: "Ищу работника",
  typography,
};

export const userNameObj = {
  heading: "Логин",
  type: "text",
  placeholder: "Login",
  name: "username",
  size: "small",
  htmlTag,
  typography,
  autocomplete: "username",
};

export const privacyBlockObj = {
  htmlBlock: "privacy-block",
};

export const spanPrivacyObj = {
  htmlBlock: "privacy-block__span",
  typography: "heading-sub-information",
};

export const checkBoxPrivacyObj = {
  htmlBlock: "privacy-block__check-box",
  name: "privacy",
};
export const linkPrivacyObj = {
  htmlBlock: "privacy-block__link",
  link: "/privacy",
  description: " политикой конфиденциальности",
  htmlModel: "heading-sub-information",
};

export const passwordObj = {
  heading: "Придумайте пароль",
  type: "password",
  placeholder: "******",
  name: "password",
  size: "small",
  htmlTag,
  typography,
  autocomplete: "new-password",
};

export const password2Obj = {
  heading: "Повторите пароль",
  type: "password",
  placeholder: "******",
  name: "password2",
  size: "small",
  htmlTag,
  typography,
  autocomplete: "new-password",
};

export const phoneObj = {
  heading: "Телефон",
  type: "number",
  placeholder: "Введите телефон",
  name: "phone",
  size: "small",
  htmlTag,
  typography,
};

export const emailObj = {
  heading: "E-mail",
  type: "text",
  placeholder: "Введите E-mail",
  name: "email",
  size: "small",
  htmlTag,
  typography,
};

export const buttonSubmitObj = {
  description: "ЗАРЕГИСТРИРОВАТЬСЯ",
  htmlName: "registration-submit",
};

export const spanLoadingObj = {
  htmlBlock: "auth-loader disable-button",
};

export const buttonErrorObj = {
  description: "ОК",
  htmlName: "error",
};

export const serverErrorObj = {
  Server: "Ошибка в соединении с сервером, попробуйте позднее",
};
