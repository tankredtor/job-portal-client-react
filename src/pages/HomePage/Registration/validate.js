export default function validateInfo(values) {
  let errors = {};
  const beginWithoutDigit = /^\D.*$/;
  const withoutSpecialChars = /^[^()$ /]*$/;
  const containsLetters = /^.*[a-zA-Z]+.*$/;

  if (!values.username.trim()) {
    errors.username = "Вы не придумали Логин";
  } else if (!values.username.length > 64) {
    errors.username = "Ограничение на ввод 64 символа";
  }

  if (!values.email) {
    errors.email = "Вы не указали E-mail";
  } else if (!/\S+@\S+\.\S+/.test(values.email)) {
    errors.email = "Введен некорректный E-mail";
  } else if (values.email.length > 64) {
    errors.email = "Ограничение на ввод 64 символа";
  }

  if (!values.password) {
    errors.password = "Вы не ввели пароль";
  } else if (values.password.length < 6) {
    errors.password = "Пароль должен состоять минимум из 6 символов";
  } else if (values.password.length > 64) {
    errors.password = "Пароль не может быть больше 64 символов и содеражать";
  } else if (
    !beginWithoutDigit.test(values.password) ||
    !withoutSpecialChars.test(values.password) ||
    !containsLetters.test(values.password)
  ) {
    errors.password =
      "Пароль должен состоять минимум из 1 буквы, не содержать кириллицу и специальные символы и пробелы";
  } else if (values.password.length > 64) {
    errors.password = "Ограничение на ввод 64 символа";
  }

  if (!values.password2 && values.password) {
    errors.password2 = "Вы должны повторить пароль";
  } else if (values.password2 !== values.password) {
    errors.password2 = "Пароли не совпадают";
  }

  if (!values.phone) {
    errors.phone = "Вы не ввели номер";
  } else if (values.phone.length > 64) {
    errors.phone = "Ограничение на ввод 64 символа";
  }

  if (!values.roleName) {
    errors.roleName = "Вы не выбрали цель регистрации";
  }

  if (!values.privacy) {
    errors.privacy =
      "Чтобы завершить регистрацию вы должны согласиться с политикой конфиденциальности.";
  }

  return errors;
}
