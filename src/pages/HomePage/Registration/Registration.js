import React, { useEffect, useState, useContext, useCallback } from 'react';
import useForm from '../../../hooks/useForm';
import useErrors from '../../../hooks/useErrors';
import { AuthContext } from '../../../context/AuthContext';
import validate from './validate';
import errorsForBanner from './errorBannerMessages';
import { ApiRequests } from '../../../api/api.requests';
import { FormComponent } from '../../../Components/Form/FormComponent';
import { InputElement } from '../../../Components/Form/InputElement';
import { RadioElement } from '../../../Components/Form/RadioElement';
import { RadioFormComponent } from '../../../Components/Form/RadioFormComponent';
import { ErrorElement } from '../../../Components/Form/ErrorElement';
import { ErrorBannerComponent } from '../../../Components/Banner/ErrorBannerComponent';
import Loader from '../../../styles/images/Loader/Spinner-1s-200px.svg';
import { SpanComponent } from '../../../Components/ReusableComponents/PresenatationComponents/SpanComponent';
import { BlockComponent } from '../../../Components/BlockComponent/BlockComponent';
import { CheckBoxComponent } from '../../../Components/ReusableComponents/ActiveComponents/CheckboxComponent';
import { LinkComponent } from '../../../Components/ReusableComponents/ActiveComponents/LinkComponent';
import { ButtonStandartComponent } from '../../../Components/Buttons/ButtonStandartComponent';
import {
  FormObj,
  radioApplicantObj,
  radioEmployerObj,
  emailObj,
  password2Obj,
  passwordObj,
  phoneObj,
  userNameObj,
  buttonSubmitObj,
  buttonErrorObj,
  serverErrorObj,
  spanLoadingObj,
  privacyBlockObj,
  checkBoxPrivacyObj,
  spanPrivacyObj,
  linkPrivacyObj,
} from './Data';

export const RegistrationComponent = () => {
  const auth = useContext(AuthContext);
  const { postApiAuth, loading } = ApiRequests();
  const [form, setForm] = useState({
    username: '',
    password: '',
    password2: '',
    email: '',
    phone: '',
    roleName: '',
    privacy: false,
  });
  const { errorMessage, setServerErrors, hideBannerHandler } = useErrors(
    errorsForBanner
  );
  const {
    handleSubmit,
    setIsSubmitted,
    errors,
    isSubmitted,
    changeHandler,
    enterSubmit,
  } = useForm(form, setForm, validate);

  const registerHandler = useCallback(async () => {
    await postApiAuth(
      {
        username: form.username,
        password: form.password,
        phone: form.phone,
        email: form.email,
        roleName: form.roleName,
      },
      '/Account/Register'
    ).then(function (data) {
      if (data) {
        const { accessToken, username, roles, id, errors } = data.data;
        if (accessToken) {
          auth.login(accessToken, username, roles[0], id);
        }
        setServerErrors(errors);
      } else {
        setServerErrors({ ...serverErrorObj });
      }
    });
  }, [auth, postApiAuth, form, setServerErrors]);

  const onEnterPress = (event) => {
    if (event.key === 'Enter') {
      enterSubmit();
    }
  };
  useEffect(() => {
    if (isSubmitted) {
      registerHandler();
      setIsSubmitted(false);
    }
  }, [isSubmitted, registerHandler, setIsSubmitted]);

  return (
    <FormComponent onKeyPress={onEnterPress} {...FormObj}>
      {errorMessage && (
        <ErrorBannerComponent {...errorMessage}>
          {' '}
          <ButtonStandartComponent
            {...buttonErrorObj}
            onClick={hideBannerHandler}
          />{' '}
        </ErrorBannerComponent>
      )}

      <InputElement
        {...userNameObj}
        value={form.username}
        onChange={changeHandler}
      >
        {errors.username && <ErrorElement error={errors.username} />}
      </InputElement>
      <InputElement
        {...passwordObj}
        value={form.password}
        onChange={changeHandler}
      >
        {errors.password && <ErrorElement error={errors.password} />}
      </InputElement>
      <InputElement
        {...password2Obj}
        value={form.password2}
        onChange={changeHandler}
      >
        {errors.password2 && <ErrorElement error={errors.password2} />}
      </InputElement>
      <InputElement {...phoneObj} value={form.phone} onChange={changeHandler}>
        {errors.phone && <ErrorElement error={errors.phone} />}
      </InputElement>
      <InputElement {...emailObj} value={form.email} onChange={changeHandler}>
        {errors.email && <ErrorElement error={errors.email} />}
      </InputElement>
      <RadioFormComponent>
        <RadioElement
          {...radioApplicantObj}
          onChange={changeHandler}
          checked={form.roleName === 'Applicant'}
        />
        <RadioElement
          {...radioEmployerObj}
          onChange={changeHandler}
          checked={form.roleName === 'Employer'}
        />
        {errors.roleName && <ErrorElement error={errors.roleName} />}
      </RadioFormComponent>
      <BlockComponent {...privacyBlockObj}>
        <CheckBoxComponent
          {...checkBoxPrivacyObj}
          onChange={changeHandler}
          checked={form.privacy}
        />
        <SpanComponent {...spanPrivacyObj}>
          Даю согласие на обработку персональных данных и соглашаюсь с{' '}
          <LinkComponent {...linkPrivacyObj} />
        </SpanComponent>
      </BlockComponent>
      {errors.privacy && <ErrorElement error={errors.privacy} />}

      {!loading && (
        <ButtonStandartComponent {...buttonSubmitObj} onClick={handleSubmit} />
      )}

      {loading && (
        <SpanComponent {...spanLoadingObj}>
          <Loader />
        </SpanComponent>
      )}
    </FormComponent>
  );
};
