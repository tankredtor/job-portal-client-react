export default function errorsforBanner(errorMessage) {
  let errors = {};

  if (
    errorMessage.includes("Wrong password for account.") ||
    errorMessage.includes("User not found.")
  ) {
    errors.heading = "Ошибка при входе";
    errors.body = "Введен неверный логин или пароль";
  }
  if (
    errorMessage.includes("Ошибка в соединении с сервером, попробуйте позднее")
  ) {
    errors.heading = "Ошибка";
    errors.body = "Ошибка в соединении с сервером, попробуйте позднее";
  }

  return errors;
}
