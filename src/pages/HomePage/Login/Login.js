import React, { useContext, useEffect, useState, useCallback } from 'react';
import { AuthContext } from '../../../context/AuthContext';
import useErrors from '../../../hooks/useErrors';
import { ApiRequests } from '../../../api/api.requests';
import useForm from '../../../hooks/useForm';
import { FormComponent } from '../../../Components/Form/FormComponent';
import { InputElement } from '../../../Components/Form/InputElement';
import errorsForBanner from './errorBannerMessages';
import { ErrorElement } from '../../../Components/Form/ErrorElement';
import { ButtonStandartComponent } from '../../../Components/Buttons/ButtonStandartComponent';
import { ErrorBannerComponent } from '../../../Components/Banner/ErrorBannerComponent';
import Loader from '../../../styles/images/Loader/Spinner-1s-200px.svg';
import { SpanComponent } from '../../../Components/ReusableComponents/PresenatationComponents/SpanComponent';
import validate from './validate';
import {
  FormObj,
  userNameObj,
  passwordObj,
  buttonSubmitObj,
  serverErrorObj,
  buttonErrorObj,
  spanLoadingObj,
} from './Data';

export const LoginComponent = () => {
  const auth = useContext(AuthContext);
  const { postApiAuth, loading } = ApiRequests();
  const [form, setForm] = useState({
    username: '',
    password: '',
  });
  const { errorMessage, setServerErrors, hideBannerHandler } = useErrors(
    errorsForBanner
  );
  const {
    handleSubmit,
    setIsSubmitted,
    errors,
    isSubmitted,
    changeHandler,
    enterSubmit,
  } = useForm(form, setForm, validate);

  const onEnterPress = (event) => {
    if (event.key === 'Enter') {
      enterSubmit();
    }
  };

  const loginHandler = useCallback(async () => {
    await postApiAuth(
      {
        username: form.username,
        password: form.password,
      },
      '/Account/Login'
    ).then(function (data) {
      if (data) {
        const { accessToken, username, roles, id, errors } = data.data;
        if (accessToken) {
          auth.login(accessToken, username, roles[0], id);
        }
        setServerErrors(errors);
      } else {
        setServerErrors({ ...serverErrorObj });
      }
    });
  }, [auth, postApiAuth, form, setServerErrors]);

  useEffect(() => {
    if (isSubmitted) {
      loginHandler();
      setIsSubmitted(false);
    }
  }, [isSubmitted, loginHandler, setIsSubmitted]);

  return (
    <FormComponent onKeyPress={onEnterPress} {...FormObj}>
      {errorMessage && (
        <ErrorBannerComponent {...errorMessage}>
          {' '}
          <ButtonStandartComponent
            {...buttonErrorObj}
            onClick={hideBannerHandler}
          />{' '}
        </ErrorBannerComponent>
      )}
      <InputElement
        {...userNameObj}
        value={form.username}
        onChange={changeHandler}
      >
        {errors.username && <ErrorElement error={errors.username} />}
      </InputElement>
      <InputElement
        {...passwordObj}
        value={form.password}
        onChange={changeHandler}
      >
        {errors.password && <ErrorElement error={errors.password} />}
      </InputElement>
      {!loading && (
        <ButtonStandartComponent {...buttonSubmitObj} onClick={handleSubmit} />
      )}
      {loading && (
        <SpanComponent {...spanLoadingObj}>
          <Loader />
        </SpanComponent>
      )}
    </FormComponent>
  );
};
