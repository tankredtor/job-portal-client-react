const htmlTag = "registration";
const typography = "registration";

export const FormObj = {
  htmlTag,
};

export const userNameObj = {
  heading: "Введите логин",
  type: "text",
  placeholder: "Login",
  name: "username",
  size: "small",
  htmlTag,
  typography,
  autocomplete: "username",
};

export const passwordObj = {
  heading: "Введите пароль",
  type: "password",
  placeholder: "******",
  name: "password",
  size: "small",
  htmlTag,
  typography,
  autocomplete: "current-password",
};

export const buttonSubmitObj = {
  description: "ВОЙТИ",
  htmlName: "login-submit",
};

export const spanLoadingObj = {
  htmlBlock: "auth-loader disable-button",
};

export const buttonErrorObj = {
  description: "ОК",
  htmlName: "error",
};

export const serverErrorObj = {
  Server: "Ошибка в соединении с сервером, попробуйте позднее",
};
