export default function validateInfo(values) {
  let errors = {};

  if (!values.username.trim()) {
    errors.username = "Вы не ввели Логин";
  }
  if (values.username && values.username.length > 64) {
    errors.username = "Логин не может быть больше 64 символов";
  }

  if (!values.password) {
    errors.password = "Вы не ввели пароль";
  }
  if (values.password && values.password.length > 64) {
    errors.password = "Пароль не может быть больше 64 символов";
  }
  return errors;
}
