import React, { useState } from "react";
import { ApplicantSvg } from "./Images/ApplicantSvg";
import { EmployerSvg } from "./Images/EmployerSvg";
import { LoginComponent } from "./Login/Login";
import { RegistrationComponent } from "./Registration/Registration";
import { LogoComponentAlumni } from "../../Components/Logo/LogoComponentAlumni";
import { LogoComponentProject } from "../../Components/Logo/LogoComponentProject";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import { UserCard } from "../../Components/UsersInfoHomePage/UserCard";
import { PageWhrapperComponent } from "../../Components/PageWhrapper/PageWhrapperComponent";
import { TiArrowBack } from "react-icons/ti";

import {
  pageObjHome,
  logoAlumniObjHome,
  logoProjectObjHome,
  usersObjHome,
  applicantCardObj,
  employerCardObj,
  whrapperInformationObjHome,
  ButtonRegistrationObjHome,
  LogoObjHome,
  validationBlockObjHome,
  validationButtonsObjHome,
} from "./Data";

export const HomePage = () => {
  const [registrationButton, setRegistrationButton] = useState(false);
  const registrationPressHandlerButton = (e) => {
    e.preventDefault();
    setRegistrationButton(true);
  };

  const loginPressHandlerButton = (e) => {
    e.preventDefault();
    setRegistrationButton(false);
  };

  return (
    <PageWhrapperComponent {...pageObjHome}>
      <BlockComponent {...LogoObjHome}>
        <LogoComponentAlumni {...logoAlumniObjHome} />
        <LogoComponentProject {...logoProjectObjHome} />
      </BlockComponent>
      <BlockComponent style={{ color: "red" }} {...whrapperInformationObjHome}>
        <BlockComponent {...usersObjHome}>
          {" "}
          <UserCard {...applicantCardObj}>
            <ApplicantSvg />{" "}
          </UserCard>
          <UserCard {...employerCardObj}>
            <EmployerSvg />{" "}
          </UserCard>
        </BlockComponent>
      </BlockComponent>
      <BlockComponent {...validationBlockObjHome}>
        <BlockComponent {...validationButtonsObjHome}>
          {registrationButton && (
            <TiArrowBack
              className="arrow-back"
              size="50"
              onClick={loginPressHandlerButton}
            />
          )}
          {!registrationButton && (
            <ButtonStandartComponent
              {...ButtonRegistrationObjHome}
              onClick={registrationPressHandlerButton}
            />
          )}
        </BlockComponent>

        {registrationButton && (
          <RegistrationComponent
            setRegistrationButton={setRegistrationButton}
          />
        )}
        {!registrationButton && <LoginComponent />}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
