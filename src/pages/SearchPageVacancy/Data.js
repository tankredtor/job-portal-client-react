export const searchPageObj = {
  htmlName: "search",
};
export const headingPageBlockObj = {
  htmlBlock: "heading-page__mobile",
};
export const headingPageObj = {
  htmlBlock: "heading-page__heading",
  typography: "heading-block-biggest",
  body: "Поиск по вакансиям",
};
export const searchBlockObj = {
  htmlBlock: "search-block",
};

export const searchSearchFieldBlockObj = {
  htmlBlock: "search-block__search",
};

export const paginationSizeBlockObj = {
  htmlBlock: "pagination-size-block",
};

export const cardsBlockObj = {
  htmlBlock: "cards-block",
};

export const paginationButtonsUpperBlockObj = {
  htmlBlock: "pagination-buttons-block__upper",
};

export const paginationButtonsLowerBlockObj = {
  htmlBlock: "pagination-buttons-block__lower",
};

export const FilterBlockObj = {
  htmlBlock: "filter-block",
};

export const FilterMobileBlockObj = {
  htmlBlock: "filter-mobile-block",
};

export const buttonShowFilterObj = {
  description: "Фильтр",
  htmlName: "filter-mobile-show",
};

export const inputSearchObj = {
  htmlBlock: "search-block__search__input",
  placeholder: "Поиск по вакансиям",
  name: "name",
  type: "text",
};
