import React, { useEffect, useState, useCallback } from "react";
import { ApiRequests } from "../../api/api.requests";
import { SortingBarComponent } from "../../Components/SearchPage/SortingBar/SortingBarComponent";
import { PaginationButtonsComponent } from "../../Components/SearchPage/PaginationButtons/PaginationButtonsComponent";
import { PaginationSizeComponent } from "../../Components/SearchPage/PaginationSize/PaginationSizeComponent";
import { NoVacancy } from "../../Components/SearchPage/VacanciesBlock/NoVacancy";
import { FilterSearchComponent } from "../../Components/SearchPage/FilterBar/FilterSearchComponent ";
import { VacanciesBlockSearchShow } from "../../Components/SearchPage/VacanciesBlock/VacanciesBlockSearchShow";
import { PageWhrapperComponent } from "../../Components/PageWhrapper/PageWhrapperComponent";
import { BlockComponent } from "../../Components/BlockComponent/BlockComponent";
import { InputComponent } from "../../Components/ReusableComponents/ActiveComponents/InputComponent";
import { ButtonStandartComponent } from "../../Components/Buttons/ButtonStandartComponent";
import { BiSearchAlt } from "react-icons/bi";
import Loader from "../../styles/images/Loader/Spinner-1s-200px.svg";
import { HeadingComponent } from "../../Components/ReusableComponents/PresenatationComponents/HeadingComponent";
import {
  searchPageObj,
  cardsBlockObj,
  paginationSizeBlockObj,
  searchBlockObj,
  searchSearchFieldBlockObj,
  paginationButtonsLowerBlockObj,
  paginationButtonsUpperBlockObj,
  FilterBlockObj,
  inputSearchObj,
  FilterMobileBlockObj,
  buttonShowFilterObj,
  headingPageBlockObj,
  headingPageObj,
} from "./Data";

export const SearchPageVacancy = () => {
  const { getApiDataFilterVacancy, loading } = ApiRequests();
  const [vacancies, setVacancies] = useState(null);
  const [filteringName, setFilteringName] = useState("");
  const [filteringSalaryFrom, setFilteringSalaryFrom] = useState(null);
  const [filteringSalaryTo, setFilteringSalaryTo] = useState(null);
  const [
    filteringRequiredExperience,
    setFilteringRequiredExperience,
  ] = useState(null);
  const [sortingByAscending, setSortingByAscending] = useState(false);
  const [sortingFieldName, setSortingFieldName] = useState(null);
  const [paramsPaginationPageSize, setParamsPaginationPageSize] = useState(
    null
  );
  const [page, setPage] = useState(1);
  const [pages, setPages] = useState(null);
  const [reloadFilter, setReloadFIlter] = useState(false);
  const [showFilter, setShowFilter] = useState(false);
  const [sorting, setSorting] = useState(false);
  //on Submit Enter

  const onEnterPress = (event) => {
    if (event.key === "Enter") {
      searchHandler();
    }
  };

  //filter button
  const filterButton = (e) => {
    e.preventDefault();
    // filter();
  };

  const pressSearchHandler = (e) => {
    e.preventDefault();
    searchHandler();
  };

  const buttonShowFilter = (e) => {
    e.preventDefault();
    setShowFilter(!showFilter);
  };

  //////////////////////////////First render//////////////////////////////

  useEffect(() => {
    window.scrollTo(0, 0);
    getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy");
  }, [getApiDataFilterVacancy]);

  //////////////////////////////Search and Filter//////////////////////////////

  const searchHandler = useCallback(() => {
    getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy", {
      "Filtering.Name": filteringName,
      "Filtering.SalaryFrom": filteringSalaryFrom,
      "Filtering.SalaryTo": filteringSalaryTo,
      "Sorting.SortingField": sortingFieldName,
      "Sorting.ByAscending": sortingByAscending,
      "Filtering.RequiredExperience": filteringRequiredExperience,
      "Pagination.PageSize": paramsPaginationPageSize,
    });
  }, [
    getApiDataFilterVacancy,
    filteringSalaryFrom,
    filteringSalaryTo,
    sortingFieldName,
    filteringName,
    sortingByAscending,
    filteringRequiredExperience,
    paramsPaginationPageSize,
  ]);

  //////////////////////////////Sorting//////////////////////////////

  useEffect(() => {
    if (sorting) {
      getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy", {
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Filtering.RequiredExperience": filteringRequiredExperience,
        "Pagination.PageSize": paramsPaginationPageSize,
      });
    }
    return () => {
      setSorting(false);
    };
  }, [sortingByAscending, sortingFieldName, getApiDataFilterVacancy]);

  //////////////////////////////Page Switch //////////////////////////////

  const paginationSwitchHandler = useCallback(
    (pagination) => {
      getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy", {
        "Pagination.Page": pagination,
        "Pagination.PageSize": paramsPaginationPageSize,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.RequiredExperience": filteringRequiredExperience,
      });
      window.scrollTo({
        top: 0,
        behavior: "smooth",
      });
    },
    [
      getApiDataFilterVacancy,
      paramsPaginationPageSize,
      filteringName,
      filteringSalaryFrom,
      filteringSalaryTo,
      sortingByAscending,
      sortingFieldName,
      filteringRequiredExperience,
    ]
  );

  //////////////////////////////Page Size//////////////////////////////

  const paginationSizeHandler = useCallback(
    (size) => {
      getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy", {
        "Pagination.Page": page,
        "Pagination.PageSize": size,
        "Filtering.Name": filteringName,
        "Filtering.SalaryFrom": filteringSalaryFrom,
        "Filtering.SalaryTo": filteringSalaryTo,
        "Sorting.SortingField": sortingFieldName,
        "Sorting.ByAscending": sortingByAscending,
        "Filtering.RequiredExperience": filteringRequiredExperience,
      });
    },
    [
      getApiDataFilterVacancy,
      filteringName,
      page,
      filteringSalaryFrom,
      filteringSalaryTo,
      sortingByAscending,
      sortingFieldName,
      filteringRequiredExperience,
    ]
  );

  ////////////////////////////// Reloading Filter //////////////////////////////

  useEffect(() => {
    if (reloadFilter) {
      setFilteringName("");
      setFilteringSalaryFrom(null);
      setFilteringSalaryTo(null);
      setSortingByAscending(false);
      setSortingFieldName(null);
      setFilteringRequiredExperience(null);
      getApiDataFilterVacancy(setVacancies, setPage, setPages, "/Vacancy", {
        "Pagination.PageSize": paramsPaginationPageSize,
      });
    }

    return () => {
      setReloadFIlter(false);
    };
  }, [reloadFilter, getApiDataFilterVacancy]);

  return (
    <PageWhrapperComponent {...searchPageObj}>
      <BlockComponent {...headingPageBlockObj}>
        <HeadingComponent {...headingPageObj} />
      </BlockComponent>
      <BlockComponent {...searchBlockObj}>
        <BlockComponent {...searchSearchFieldBlockObj}>
          <InputComponent
            {...inputSearchObj}
            value={filteringName}
            onChange={(e) => setFilteringName(e.target.value)}
            onKeyPress={onEnterPress}
          />
          <BiSearchAlt
            className="search-block__search__icon"
            size="40"
            onClick={pressSearchHandler}
          />
        </BlockComponent>

        <SortingBarComponent
          setSortingFieldName={setSortingFieldName}
          setSortingByAscending={setSortingByAscending}
          setSorting={setSorting}
        />
      </BlockComponent>

      <BlockComponent {...paginationButtonsUpperBlockObj}>
        {!loading && vacancies && (
          <PaginationButtonsComponent
            page={page}
            pages={pages}
            paginationSwitchHandler={paginationSwitchHandler}
          />
        )}
      </BlockComponent>
      <BlockComponent {...cardsBlockObj}>
        {loading && <Loader />}
        {!loading && vacancies && (
          <VacanciesBlockSearchShow vacancies={vacancies} />
        )}
        {!loading && !vacancies && <NoVacancy />}
      </BlockComponent>

      <BlockComponent {...paginationSizeBlockObj}>
        {!loading && vacancies && (
          <PaginationSizeComponent
            paginationSizeHandler={paginationSizeHandler}
            setParamsPaginationPageSize={setParamsPaginationPageSize}
          />
        )}
      </BlockComponent>

      <BlockComponent {...paginationButtonsLowerBlockObj}>
        {!loading && vacancies && (
          <PaginationButtonsComponent
            page={page}
            pages={pages}
            paginationSwitchHandler={paginationSwitchHandler}
          />
        )}
      </BlockComponent>
      <BlockComponent {...FilterBlockObj}>
        <FilterSearchComponent
          valueSalaryFrom={filteringSalaryFrom}
          salaryFromChangeFunc={setFilteringSalaryFrom}
          valueSalaryTo={filteringSalaryTo}
          salaryToChangeFunc={setFilteringSalaryTo}
          valueFilteringRequiredExperience={filteringRequiredExperience}
          filterButton={filterButton}
          setFilteringRequiredExperience={setFilteringRequiredExperience}
          searchHandler={searchHandler}
          setReloadFilter={setReloadFIlter}
        />
      </BlockComponent>

      <BlockComponent {...FilterMobileBlockObj}>
        {!showFilter && (
          <ButtonStandartComponent
            {...buttonShowFilterObj}
            onClick={buttonShowFilter}
          />
        )}

        {showFilter && (
          <FilterSearchComponent
            valueSalaryFrom={filteringSalaryFrom}
            salaryFromChangeFunc={setFilteringSalaryFrom}
            valueSalaryTo={filteringSalaryTo}
            salaryToChangeFunc={setFilteringSalaryTo}
            valueFilteringRequiredExperience={filteringRequiredExperience}
            filterButton={filterButton}
            setFilteringRequiredExperience={setFilteringRequiredExperience}
            searchHandler={searchHandler}
            setReloadFilter={setReloadFIlter}
            showFilter={buttonShowFilter}
          />
        )}
      </BlockComponent>
    </PageWhrapperComponent>
  );
};
