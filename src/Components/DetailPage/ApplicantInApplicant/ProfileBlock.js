import React, { useContext } from "react";
import { AuthContext } from "../../../context/AuthContext";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { ALinkComponent } from "../../ReusableComponents/ActiveComponents/ALinkComponent";
import { Icon24LogoVk } from "@vkontakte/icons";
import { FaFacebookSquare } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";
import { FaPen } from "react-icons/fa";
import { LinkComponent } from "../../ReusableComponents/ActiveComponents/LinkComponent";
import {
  nameBlockProfileObj,
  dataInformationProfileObj,
  dataLastnameProfileObj,
  dataNameProfileObj,
  informationBlockProfileObj,
  spanCitizenshipProfileObj,
  spanCityProfileObj,
  spanGenderProfileObj,
  blockProfileObj,
  informationBlockProfileMainObj,
  aLinkProfileObj,
  linkChangeProfileObj,
} from "./Data";

export const ProfileBlock = ({ userInfo }) => {
  const { userRole } = useContext(AuthContext);
  const genderInfo = (gender) => {
    if (gender === "man") {
      return "Мужской";
    }
    return "Женский";
  };

  const limitCharacters = (title, limit = 26) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split("").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join("")}...`;
    }
    return title;
  };
  return (
    <BlockComponent {...blockProfileObj}>
      {userRole === "Admin" && (
        <LinkComponent
          {...linkChangeProfileObj}
          link={`/change-applicant/${userInfo.id}`}
        >
          <FaPen className="block-card__link-change__icon" size="25" />
        </LinkComponent>
      )}
      <BlockComponent {...nameBlockProfileObj}>
        <SpanComponent {...dataNameProfileObj} body={userInfo.firstName} />
        <SpanComponent {...dataLastnameProfileObj} body={userInfo.lastName} />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileObj}>
        <SpanComponent {...spanGenderProfileObj} />
        <SpanComponent
          {...dataInformationProfileObj}
          body={genderInfo(userInfo.gender)}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileObj}>
        <SpanComponent {...spanCitizenshipProfileObj} />
        <SpanComponent
          {...dataInformationProfileObj}
          body={userInfo.citizenship}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileObj}>
        <SpanComponent {...spanCityProfileObj} />
        <SpanComponent {...dataInformationProfileObj} body={userInfo.city} />
      </BlockComponent>
      {userInfo.contactPhone && (
        <BlockComponent {...informationBlockProfileMainObj}>
          <ALinkComponent
            {...aLinkProfileObj}
            body={limitCharacters(userInfo.contactPhone)}
            href={`tel:${userInfo.contactPhone}`}
          />
        </BlockComponent>
      )}
      <BlockComponent {...informationBlockProfileMainObj}>
        <ALinkComponent
          {...aLinkProfileObj}
          body={limitCharacters(userInfo.contactEmail)}
          href={`mailto:${userInfo.contactEmail}?subject=Вы выставляли резюме на Карьерном центре МГЮА`}
        />
      </BlockComponent>
      {userInfo.instagramLink && (
        <BlockComponent {...informationBlockProfileObj}>
          <ALinkComponent {...aLinkProfileObj} href={userInfo.instagramLink}>
            <AiFillInstagram
              className="block-applicant-profile__social-icons__instagram block-applicant-profile__social-icons__icon"
              size="60"
            />
          </ALinkComponent>
        </BlockComponent>
      )}

      {userInfo.vkLink && (
        <BlockComponent {...informationBlockProfileObj}>
          <ALinkComponent {...aLinkProfileObj} href={userInfo.vkLink}>
            <Icon24LogoVk
              className="block-applicant-profile__social-icons__vk block-applicant-profile__social-icons__icon"
              width={60}
              height={60}
            />
          </ALinkComponent>
        </BlockComponent>
      )}

      {userInfo.facebookLink && (
        <BlockComponent {...informationBlockProfileObj}>
          <ALinkComponent {...aLinkProfileObj} href={userInfo.facebookLink}>
            <FaFacebookSquare
              className="block-applicant-profile__social-icons__facebook block-applicant-profile__social-icons__icon"
              size="51"
            />
          </ALinkComponent>
        </BlockComponent>
      )}
    </BlockComponent>
  );
};
