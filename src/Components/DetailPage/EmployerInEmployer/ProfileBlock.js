import React, { useContext } from "react";
import { AuthContext } from "../../../context/AuthContext";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { ALinkComponent } from "../../ReusableComponents/ActiveComponents/ALinkComponent";
import { LinkComponent } from "../../ReusableComponents/ActiveComponents/LinkComponent";
import { FaPen } from "react-icons/fa";
import {
  blockProfileObj,
  nameBlockProfileObj,
  dataNameProfileObj,
  descriptionBlockProfileObj,
  dataDescriptionProfileObj,
  aLinkProfileObj,
  informationBlockProfileObj,
  aLinkEmailProfileObj,
  informationBlockProfileMainObj,
  linkChangeProfileObj,
  aLinkPhoneProfileObj,
} from "./Data";

export const ProfileBlock = ({ userInfo }) => {
  const limitCharacters = (title, limit = 26) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split("").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join("")}...`;
    }
    return title;
  };

  const { userRole } = useContext(AuthContext);
  return (
    <BlockComponent {...blockProfileObj}>
      {userRole === "Admin" && (
        <LinkComponent
          {...linkChangeProfileObj}
          link={`/change-employer/${userInfo.id}`}
        >
          <FaPen className="block-card__link-change__icon" size="25" />
        </LinkComponent>
      )}
      <BlockComponent {...nameBlockProfileObj}>
        <SpanComponent {...dataNameProfileObj} body={userInfo.name} />
      </BlockComponent>

      <BlockComponent {...descriptionBlockProfileObj}>
        <SpanComponent
          {...dataDescriptionProfileObj}
          body={userInfo.description}
        />
      </BlockComponent>

      {userInfo.contactPhone && (
        <BlockComponent {...informationBlockProfileMainObj}>
          <ALinkComponent
            {...aLinkPhoneProfileObj}
            body={limitCharacters(userInfo.contactPhone)}
            href={`tel:${userInfo.contactPhone}`}
          />
        </BlockComponent>
      )}
      <BlockComponent {...informationBlockProfileMainObj}>
        <ALinkComponent
          {...aLinkEmailProfileObj}
          body={limitCharacters(userInfo.contactEmail)}
          href={`mailto:${userInfo.contactEmail}?subject=Вакансия на Карьерном центре МГЮА`}
        />
      </BlockComponent>

      <BlockComponent {...informationBlockProfileMainObj}>
        <ALinkComponent
          {...aLinkProfileObj}
          body={limitCharacters(userInfo.infoUrl)}
          href={userInfo.infoUrl}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileObj}></BlockComponent>
    </BlockComponent>
  );
};
