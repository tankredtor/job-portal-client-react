export const blockProfileObj = {
  htmlBlock: "block-employer-profile",
};

export const nameBlockProfileObj = {
  htmlBlock: "block-employer-profile__name",
};

export const dataNameProfileObj = {
  htmlBlock:
    "block-employer-profile__name__data block-employer-profile__name__data__name",
  typography: "heading-block-big",
};

export const descriptionBlockProfileObj = {
  htmlBlock: "block-employer-profile__description",
};

export const informationBlockProfileObj = {
  htmlBlock: "block-employer-profile__information",
};

export const informationBlockProfileMainObj = {
  htmlBlock:
    "block-employer-profile__information block-employer-profile__information__main",
};

export const dataInformationProfileObj = {
  htmlBlock: "block-employer-profile__information__data ",
  typography: "heading-block-medium",
};

export const dataDescriptionProfileObj = {
  htmlBlock: "block-employer-profile__description__data",
  typography: "heading-sub-information",
};

export const aLinkProfileObj = {
  htmlBlock: "block-employer-profile__a-link",
  htmlModel: "heading-block-medium-bold",
  target: "_blank",
};

export const aLinkEmailProfileObj = {
  htmlBlock: "block-employer-profile__a-link",
  htmlModel: "heading-block-medium-bold",
  target: "_blank",
};

export const aLinkPhoneProfileObj = {
  htmlBlock: "block-employer-profile__a-link",
  htmlModel: "heading-block-medium-bold",
  target: "_blank",
};

export const spanPhoneProfileObj = {
  htmlBlock: "block-employer-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "Телефон:",
};

export const spanEmailProfileObj = {
  htmlBlock: "block-employer-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "E-mail:",
};

export const linkChangeProfileObj = {
  htmlBlock: "block-profile__link-change__icon",
};
