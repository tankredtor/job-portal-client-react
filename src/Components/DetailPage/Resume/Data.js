export const blockCardObj = {
  htmlBlock: "block-card",
};

export const nameBlockCardObj = {
  htmlBlock: "block-card__name",
};

export const dataNameCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-biggest",
};

export const informationBlockCardObj = {
  htmlBlock: "block-card__information",
};
export const spanAboutMeCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "О соискателе",
};

export const spanEducationStatusCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Образование",
};

export const spanSalaryCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Желаемый доход",
};

export const spanExperienceCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Опыт работы",
};

export const spanDateCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-sub-information",
};

export const dataObj = {
  htmlBlock: "block-card__information__data block-card__information__data",
  typography: "heading-sub-information",
};
export const linkChangeObj = {
  htmlBlock: "block-card__link",
};
