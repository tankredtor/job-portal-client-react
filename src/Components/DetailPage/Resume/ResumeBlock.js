import React, { useContext } from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { FaPen } from "react-icons/fa";
import { AuthContext } from "../../../context/AuthContext";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import {
  blockCardObj,
  nameBlockCardObj,
  dataNameCardObj,
  dataObj,
  informationBlockCardObj,
  spanAboutMeCardObj,
  spanEducationStatusCardObj,
  spanExperienceCardObj,
  spanSalaryCardObj,
  linkChangeObj,
  spanDateCardObj,
} from "./Data";

export const ResumeBlock = ({ userInfo }) => {
  const { userId, userRole } = useContext(AuthContext);
  const salaryInfo = (data) => {
    if (data.salaryFrom && data.salaryTo) {
      return `${data.salaryFrom} - ${data.salaryTo} Руб. `;
    } else if (data.salaryFrom && !data.salaryTo) {
      return `от ${data.salaryFrom} Руб.`;
    } else if (data.salaryTo && !data.salaryFrom) {
      return ` до ${data.salaryTo} Руб.`;
    } else if (data.salaryTo === 0 && data.salaryTo === data.salaryFrom) {
      return "Доход не указан";
    } else return "Доход не указан";
  };

  const educationInfo = (data) => {
    if (data.educationStatus === 0) {
      return "Обучаюсь на Бакалавриате";
    } else if (data.educationStatus === 1) {
      return "Оконченный бакалавриат";
    } else if (data.educationStatus === 2) {
      return "Обучаюсь на специалитете";
    } else if (data.educationStatus === 3) {
      return "Оконченный специалитет";
    } else if (data.educationStatus === 4) {
      return "Обучаюсь в магистратуре";
    } else if (data.educationStatus === 5) {
      return "Оконченная магистратура";
    } else if (data.educationStatus === 6) {
      return "Аспирант";
    } else if (data.educationStatus === 7) {
      return "Имею ученую степень";
    }
  };

  const experienceInfo = (data) => {
    if (data.experience === 0) {
      return "Без опыта";
    } else if (data.experience === 1) {
      return "От 1 года до 3 лет";
    } else if (data.experience === 2) {
      return "От 3 до 6 лет";
    } else if (data.experience === 3) {
      return "Более 6 лет";
    }
  };

  const dateOfCreationInfo = (date) => {
    const d = new Date(date);

    const currDate = d.getDate();

    const currMonth = d.getMonth() + 1;

    const currYear = d.getFullYear();

    if (currDate < 10 && currMonth < 10) {
      return "0" + currDate + ".0" + currMonth + "." + currYear;
    } else if (currDate > 10 && currMonth < 10) {
      return currDate + ".0" + currMonth + "." + currYear;
    } else if (currDate < 10 && currMonth > 10) {
      return "0" + currDate + "." + currMonth + "." + currYear;
    }
    return currDate + "." + currMonth + "." + currYear;
  };

  return (
    <BlockComponent {...blockCardObj}>
      <BlockComponent {...nameBlockCardObj}>
        <SpanComponent {...dataNameCardObj} body={userInfo.name} />
        {(userId === userInfo.applicant.userId || userRole === "Admin") && (
          <LinkComponent
            className="block-card__link-change"
            link={`/change-resume/${userInfo.id}`}
            {...linkChangeObj}
          >
            <FaPen className="block-card__link-change__icon" size="20" />
          </LinkComponent>
        )}
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent
          {...spanDateCardObj}
          body={`Резюме создано ${dateOfCreationInfo(userInfo.updatedAt)}`}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanExperienceCardObj} />
        <SpanComponent {...dataObj} body={experienceInfo(userInfo)} />
      </BlockComponent>

      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanSalaryCardObj} />
        <SpanComponent {...dataObj} body={salaryInfo(userInfo)} />
      </BlockComponent>

      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanAboutMeCardObj} />
        <SpanComponent {...dataObj} body={userInfo.aboutMe} />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanEducationStatusCardObj} />
        <SpanComponent {...dataObj} body={educationInfo(userInfo)} />
      </BlockComponent>
    </BlockComponent>
  );
};
