export const blockCardObj = {
  htmlBlock: "block-card",
};

export const nameBlockCardObj = {
  htmlBlock: "block-card__name",
};

export const dataNameCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-biggest",
};

export const informationBlockCardObj = {
  htmlBlock: "block-card__information",
};
export const spanDescriptionCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Описание вакансии",
};

export const spanDutiesCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Обязанности",
};

export const spanSalaryCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Доход",
};

export const spanExperienceCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Требуемый опыт работы",
};

export const spanRequirementsCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Требования",
};

export const spanTypeOfEmploymentCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-block-medium-bold",
  body: "Тип занятости",
};

export const spanDateCardObj = {
  htmlBlock: "block-card__information__span",
  typography: "heading-sub-information",
};

export const dataObj = {
  htmlBlock: "block-card__information__data block-card__information__data",
  typography: "heading-sub-information",
};
export const linkChangeObj = {
  htmlBlock: "block-card__link",
};
