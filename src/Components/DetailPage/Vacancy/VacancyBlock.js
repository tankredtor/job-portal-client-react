import React, { useContext } from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { FaPen } from "react-icons/fa";
import { AuthContext } from "../../../context/AuthContext";
import { LinkComponent } from "../../../Components/ReusableComponents/ActiveComponents/LinkComponent";
import {
  blockCardObj,
  nameBlockCardObj,
  dataNameCardObj,
  informationBlockCardObj,
  spanDescriptionCardObj,
  spanDutiesCardObj,
  spanSalaryCardObj,
  spanExperienceCardObj,
  spanRequirementsCardObj,
  spanTypeOfEmploymentCardObj,
  spanDateCardObj,
  dataObj,
  linkChangeObj,
} from "./Data";

export const VacancyBlock = ({ userInfo }) => {
  const { userId, userRole } = useContext(AuthContext);
  const salaryInfo = (data) => {
    if (data.salaryFrom && data.salaryTo) {
      return `${data.salaryFrom} - ${data.salaryTo} Руб. `;
    } else if (data.salaryFrom && !data.salaryTo) {
      return `от ${data.salaryFrom} Руб.`;
    } else if (data.salaryTo && !data.salaryFrom) {
      return ` до ${data.salaryTo} Руб.`;
    } else if (data.salaryTo === 0 && data.salaryTo === data.salaryFrom) {
      return "Доход не указан";
    } else return "Доход не указан";
  };

  const typeOfEmploymentInfo = (data) => {
    if (data.typeOfEmployment === 0) {
      return "Частичная занятость";
    } else if (data.typeOfEmployment === 1) {
      return "Полная занятость";
    }
  };

  const experienceInfo = (data) => {
    if (data.requiredExperience === 0) {
      return "Без опыта";
    } else if (data.requiredExperience === 1) {
      return "От 1 года до 3 лет";
    } else if (data.requiredExperience === 2) {
      return "От 3 до 6 лет";
    } else if (data.requiredExperience === 3) {
      return "Более 6 лет";
    }
  };

  const dateOfCreationInfo = (date) => {
    const d = new Date(date);

    const currDate = d.getDate();

    const currMonth = d.getMonth() + 1;

    const currYear = d.getFullYear();

    if (currDate < 10 && currMonth < 10) {
      return "0" + currDate + ".0" + currMonth + "." + currYear;
    } else if (currDate > 10 && currMonth < 10) {
      return currDate + ".0" + currMonth + "." + currYear;
    } else if (currDate < 10 && currMonth > 10) {
      return "0" + currDate + "." + currMonth + "." + currYear;
    }
    return currDate + "." + currMonth + "." + currYear;
  };

  return (
    <BlockComponent {...blockCardObj}>
      <BlockComponent {...nameBlockCardObj}>
        <SpanComponent {...dataNameCardObj} body={userInfo.name} />
        {(userId === userInfo.employer.userId || userRole === "Admin") && (
          <LinkComponent
            className="block-card__link-change"
            link={`/change-vacancy/${userInfo.id}`}
            {...linkChangeObj}
          >
            <FaPen className="block-card__link-change__icon" size="25" />
          </LinkComponent>
        )}
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent
          {...spanDateCardObj}
          body={`Вакансия создана ${dateOfCreationInfo(userInfo.updatedAt)}`}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanSalaryCardObj} />
        <SpanComponent {...dataObj} body={salaryInfo(userInfo)} />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanExperienceCardObj} />
        <SpanComponent {...dataObj} body={experienceInfo(userInfo)} />
      </BlockComponent>

      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanTypeOfEmploymentCardObj} />
        <SpanComponent {...dataObj} body={typeOfEmploymentInfo(userInfo)} />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanDescriptionCardObj} />
        <SpanComponent {...dataObj} body={userInfo.description} />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanDutiesCardObj} />
        <SpanComponent {...dataObj} body={userInfo.duties} />
      </BlockComponent>
      <BlockComponent {...informationBlockCardObj}>
        <SpanComponent {...spanRequirementsCardObj} />
        <SpanComponent {...dataObj} body={userInfo.requirements} />
      </BlockComponent>
    </BlockComponent>
  );
};
