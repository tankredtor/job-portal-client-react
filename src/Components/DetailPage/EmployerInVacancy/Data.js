export const blockProfileObj = {
  htmlBlock: "block-employer-profile",
};

export const nameBlockProfileObj = {
  htmlBlock: "block-employer-profile__name",
};

export const dataNameProfileObj = {
  htmlBlock: "block-employer-profile__name__data",
  typography: "heading-block-big",
};
