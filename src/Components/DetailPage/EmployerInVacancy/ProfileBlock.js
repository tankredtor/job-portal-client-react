import React from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import {
  nameBlockProfileObj,
  dataNameProfileObj,
  blockProfileObj,
} from "./Data";

export const ProfileBlock = ({ userInfo }) => {
  return (
    <BlockComponent {...blockProfileObj}>
      <BlockComponent {...nameBlockProfileObj}>
        <SpanComponent {...dataNameProfileObj} body={userInfo.name} />
      </BlockComponent>
    </BlockComponent>
  );
};
