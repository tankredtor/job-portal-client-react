import React from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import {
  nameBlockProfileObj,
  dataInformationProfileObj,
  dataLastnameProfileObj,
  dataNameProfileObj,
  informationBlockProfileObj,
  spanCityProfileObj,
  blockProfileObj,
} from "./Data";

export const ProfileBlock = ({ userInfo }) => {
  return (
    <BlockComponent {...blockProfileObj}>
      <BlockComponent {...nameBlockProfileObj}>
        <SpanComponent {...dataNameProfileObj} body={userInfo.firstName} />
        <SpanComponent {...dataLastnameProfileObj} body={userInfo.lastName} />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileObj}>
        <SpanComponent {...spanCityProfileObj} />
        <SpanComponent {...dataInformationProfileObj} body={userInfo.city} />
      </BlockComponent>
    </BlockComponent>
  );
};
