import React from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { ALinkComponent } from "../../ReusableComponents/ActiveComponents/ALinkComponent";
import {
  blockProfileObj,
  nameBlockProfileObj,
  dataNameProfileObj,
  descriptionBlockProfileObj,
  dataDescriptionProfileObj,
  aLinkProfileObj,
  informationBlockProfileObj,
  informationBlockProfileMainObj,
} from "./Data";

export const ProfileBlock = ({ userInfo }) => {
  const limitCharacters = (title, limit = 26) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split("").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join("")}...`;
    }
    return title;
  };
  return (
    <BlockComponent {...blockProfileObj}>
      <BlockComponent {...nameBlockProfileObj}>
        <SpanComponent {...dataNameProfileObj} body={userInfo.name} />
      </BlockComponent>

      <BlockComponent {...descriptionBlockProfileObj}>
        <SpanComponent
          {...dataDescriptionProfileObj}
          body={userInfo.description}
        />
      </BlockComponent>

      {userInfo.contactPhone && (
        <BlockComponent {...informationBlockProfileMainObj}>
          <ALinkComponent
            {...aLinkProfileObj}
            body={limitCharacters(userInfo.contactPhone)}
            href={`tel:${userInfo.contactPhone}`}
          />
        </BlockComponent>
      )}
      <BlockComponent {...informationBlockProfileMainObj}>
        <ALinkComponent
          {...aLinkProfileObj}
          body={limitCharacters(userInfo.contactEmail)}
          href={`mailto:${userInfo.contactEmail}?subject=Вакансия на Карьерном центре МГЮА`}
        />
      </BlockComponent>
      <BlockComponent {...informationBlockProfileMainObj}>
        <ALinkComponent
          {...aLinkProfileObj}
          body={limitCharacters(userInfo.infoUrl)}
          href={userInfo.infoUrl}
        />
      </BlockComponent>

      <BlockComponent {...informationBlockProfileObj}></BlockComponent>
    </BlockComponent>
  );
};
