export const blockProfileObj = {
  htmlBlock: "block-applicant-profile",
};

export const nameBlockProfileObj = {
  htmlBlock: "block-applicant-profile__name",
};

export const dataNameProfileObj = {
  htmlBlock:
    "block-applicant-profile__name__data block-applicant-profile__name__data__name",
  typography: "heading-block-big",
};

export const dataLastnameProfileObj = {
  htmlBlock:
    "block-applicant-profile__name__data block-applicant-profile__name__lastname",
  typography: "heading-block-big",
};

export const informationBlockProfileObj = {
  htmlBlock: "block-applicant-profile__information",
};

export const informationBlockProfileMainObj = {
  htmlBlock:
    "block-applicant-profile__information block-applicant-profile__information__main",
};

export const dataInformationProfileObj = {
  htmlBlock: "block-applicant-profile__information__data ",
  typography: "heading-block-medium",
};

export const spanCityProfileObj = {
  htmlBlock: "block-applicant-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "Город: ",
};

export const spanPhoneProfileObj = {
  htmlBlock: "block-applicant-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "Телефон:",
};

export const spanEmailProfileObj = {
  htmlBlock: "block-applicant-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "E-mail:",
};

export const aLinkProfileObj = {
  htmlBlock: "block-applicant-profile__social-icons",
  htmlModel: "heading-block-medium-bold",
  target: "_blank",
};

export const spanGenderProfileObj = {
  htmlBlock: "block-applicant-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "Пол:",
};

export const spanCitizenshipProfileObj = {
  htmlBlock: "block-applicant-profile__information__span",
  typography: "heading-block-medium-bold",
  body: "Гражданство:",
};
