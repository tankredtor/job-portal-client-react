import React from "react";

export const OptionElement = ({ type, value, description }) => {
  return (
    <option
      className="form-create__input__select__option"
      type={type}
      value={value}
    >
      {description}
    </option>
  );
};
