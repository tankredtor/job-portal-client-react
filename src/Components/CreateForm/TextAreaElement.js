import React from "react";

export const TextAreaElement = ({
  heading,
  type,
  placeholder,
  name,
  value,
  onChange,
  size,
  children,
}) => {
  return (
    <div className="form-create__input">
      <span className="form-create__input__label heading-sub-information">
        {heading}
      </span>
      <textarea
        placeholder={placeholder}
        id={name}
        type={type}
        name={name}
        className={`form-create__input__input-field input-field__${size}`}
        value={value}
        onChange={onChange}
      />
      {children}
    </div>
  );
};
