import React from "react";

export const SelectElement = ({
  children,
  value,
  onChange,
  heading,
  type,
  name,
}) => {
  return (
    <div className="form-create__input">
      <span className="form-create__input__label heading-sub-information">
        {" "}
        {heading}
      </span>
      <select
        className="form-create__input__select"
        name={name}
        type={type}
        value={value}
        onChange={onChange}
      >
        {children}
      </select>
    </div>
  );
};
