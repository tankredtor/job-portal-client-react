import React from "react";

export const ErrorElement = ({ error }) => {
  return (
    <span className="form-create__input__error heading-sub-information">
      {error}
    </span>
  );
};
