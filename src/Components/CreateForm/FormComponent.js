import React from "react";

export const FormComponent = ({
  children,
  headingPrimary,
  headingSecondary,
}) => {
  return (
    <>
      <div className="form-create">
        <div className="form-create__heading-primary heading-main-information">
          {headingPrimary}
        </div>
        <div className="form-create__heading-secondary heading-sub-information">
          {headingSecondary}
        </div>

        {children}
      </div>
    </>
  );
};
