import React from "react";

export const BlockComponent = ({
  children,
  htmlTag = "",
  htmlBlock = "",
  htmlElement = "",
}) => {
  return (
    <div className={`${htmlBlock}${htmlTag}${htmlElement}`}>{children}</div>
  );
};
