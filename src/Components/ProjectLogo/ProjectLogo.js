import React from "react";
import LogoAlumni from "../../styles/images/Logo/alumniLogo.png";

export const ProjectLogo = ({ htmlBlock }) => {
  return <img src={LogoAlumni} alt="Logo" className={`${htmlBlock}`} />;
};
