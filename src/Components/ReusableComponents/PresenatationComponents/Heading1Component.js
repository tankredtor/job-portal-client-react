import React from "react";

export const Heading1Component = ({
  htmlBlock = "",
  typography = "",
  body,
  onClick,
}) => {
  return (
    <h1
      onClick={onClick}
      className={`heading-component ${htmlBlock} ${typography} `}
    >
      {body}
    </h1>
  );
};
