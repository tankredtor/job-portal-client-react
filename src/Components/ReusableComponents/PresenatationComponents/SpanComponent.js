import React from "react";

export const SpanComponent = ({
  htmlBlock = "",
  typography = "",
  body,
  onClick,
  children,
}) => {
  return (
    <span
      onClick={onClick}
      className={`span-component ${htmlBlock} ${typography} `}
    >
      {body} {children}
    </span>
  );
};
