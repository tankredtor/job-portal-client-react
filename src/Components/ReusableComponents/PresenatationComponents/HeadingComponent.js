import React from "react";

export const HeadingComponent = ({
  htmlBlock = "",
  typography = "",
  body,
  onClick,
}) => {
  return (
    <h2
      onClick={onClick}
      className={`heading-component ${htmlBlock} ${typography} `}
    >
      {body}
    </h2>
  );
};
