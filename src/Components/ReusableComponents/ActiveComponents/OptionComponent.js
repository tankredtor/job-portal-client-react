import React from "react";

export const OptionComponent = ({ type, value, description, htmlBlock }) => {
  return (
    <option className={`option-component ${htmlBlock}`} value={value}>
      {description}
    </option>
  );
};
