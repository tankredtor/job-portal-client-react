import React from "react";
export const ALinkComponent = ({
  htmlBlock,
  htmlModel,
  href,
  children,
  body,
  target,
}) => {
  return (
    <a target={target} className={`${htmlBlock} ${htmlModel}`} href={href}>
      {children} {body}
    </a>
  );
};
