import React from "react";
import { Link } from "react-router-dom";

export const LinkComponent = ({
  link,
  description,
  htmlBlock,
  children,
  htmlModel,
}) => {
  return (
    <Link to={link} className={`link-component ${htmlBlock} ${htmlModel}`}>
      {description} {children}
    </Link>
  );
};
