import React from "react";

export const InputComponent = ({
  type,
  placeholder,
  name,
  value,
  onChange,
  htmlBlock,
  onKeyPress,
}) => {
  return (
    <input
      placeholder={placeholder}
      id={name}
      type={type}
      name={name}
      className={`input-component ${htmlBlock}`}
      value={value}
      onChange={onChange}
      onKeyPress={onKeyPress}
    />
  );
};
