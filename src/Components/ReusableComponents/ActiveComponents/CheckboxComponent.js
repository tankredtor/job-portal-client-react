import React from "react";

export const CheckBoxComponent = ({
  name,
  value,
  onChange,
  htmlBlock,
  onKeyPress,
  checked,
}) => {
  return (
    <input
      id={name}
      type="checkbox"
      checked={checked}
      name={name}
      className={`check-box-component ${htmlBlock}`}
      value={value}
      onChange={onChange}
      onKeyPress={onKeyPress}
    />
  );
};
