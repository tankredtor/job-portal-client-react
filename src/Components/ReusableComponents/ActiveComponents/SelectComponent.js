import React from "react";
import { MdKeyboardArrowDown } from "react-icons/md";

export const SelectComponent = ({
  children,
  value,
  onChange,
  type,
  name,
  htmlBlock,
}) => {
  return (
    <div className={`select-component ${htmlBlock}`}>
      <select
        className="select-component__select"
        name={name}
        type={type}
        value={value}
        onChange={onChange}
      >
        {children}
      </select>
      <MdKeyboardArrowDown className="select-component__arrow" size="30" />
    </div>
  );
};
