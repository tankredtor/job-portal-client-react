import React from "react";

export const PageWhrapperComponent = ({ htmlName, children }) => {
  return <div className={`${htmlName}-page`}>{children}</div>;
};
