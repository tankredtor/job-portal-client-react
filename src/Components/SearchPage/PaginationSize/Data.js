export const buttonTenObj = {
  htmlName: "page-size",
  description: "10",
  value: 10,
};

export const buttonTwenyObj = {
  htmlName: "page-size",
  description: "20",
  value: 20,
};

export const buttonTwenyFiveObj = {
  htmlName: "page-size",
  description: "25",
  value: 25,
};

export const spanObj = {
  htmlBlock: "pagination-size-block__span",
  body: "Отображать странице:",
  typography: "heading-sub-information",
};
