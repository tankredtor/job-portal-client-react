import React from "react";
import {
  buttonTenObj,
  buttonTwenyFiveObj,
  buttonTwenyObj,
  spanObj,
} from "./Data";
import { ButtonStandartComponent } from "../../Buttons/ButtonStandartComponent";
import "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";

export const PaginationSizeComponent = ({
  setParamsPaginationPageSize,
  paginationSizeHandler,
}) => {
  const changeHandlerPaginationSize = (e) => {
    e.preventDefault();
    setParamsPaginationPageSize(parseInt(e.target.value, 10));
    paginationSizeHandler(parseInt(e.target.value, 10));
  };

  return (
    <>
      <SpanComponent {...spanObj} />
      <ButtonStandartComponent
        {...buttonTenObj}
        onClick={changeHandlerPaginationSize}
      />
      <ButtonStandartComponent
        {...buttonTwenyObj}
        onClick={changeHandlerPaginationSize}
      />
      <ButtonStandartComponent
        {...buttonTwenyFiveObj}
        onClick={changeHandlerPaginationSize}
      />
    </>
  );
};
