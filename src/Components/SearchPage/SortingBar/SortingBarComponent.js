import React, { useState, useCallback, useEffect } from "react";
import { SelectComponent } from "../../ReusableComponents/ActiveComponents/SelectComponent";
import { OptionComponent } from "../../ReusableComponents/ActiveComponents/OptionComponent";
import {
  optionDateOfCreationAscending,
  optionDateOfCreationDescending,
  optionSalaryFromAscending,
  optionSalaryFromDescending,
  optionSalaryToAscending,
  optionSalaryToDescending,
  selectSortingObj,
} from "./Data";
export const SortingBarComponent = ({
  setSortingFieldName,
  setSortingByAscending,
  setSorting,
}) => {
  const [sortingNum, setSortingNum] = useState(null);
  const sortingHandler = (e) => {
    setSortingNum(parseInt(e.target.value, 10));
  };
  const sortingFilterNum = useCallback(() => {
    if (sortingNum === 6) {
      setSortingFieldName(parseInt(0));
      setSortingByAscending(false);
      setSorting(true);
    } else if (sortingNum === 1) {
      setSortingFieldName(parseInt(0));
      setSortingByAscending(true);
      setSorting(true);
    } else if (sortingNum === 2) {
      setSortingFieldName(parseInt(1));
      setSortingByAscending(false);
      setSorting(true);
    } else if (sortingNum === 3) {
      setSortingFieldName(parseInt(1));
      setSortingByAscending(true);
      setSorting(true);
    } else if (sortingNum === 4) {
      setSortingFieldName(parseInt(2));
      setSortingByAscending(false);
      setSorting(true);
    } else if (sortingNum === 5) {
      setSortingFieldName(parseInt(2));
      setSortingByAscending(true);
      setSorting(true);
    }
  }, [sortingNum, setSorting, setSortingByAscending, setSortingFieldName]);

  useEffect(() => {
    sortingFilterNum();
  }, [sortingFilterNum]);

  return (
    <SelectComponent
      {...selectSortingObj}
      value={sortingNum || undefined}
      onChange={sortingHandler}
    >
      <OptionComponent {...optionDateOfCreationDescending} value={4} />
      <OptionComponent {...optionDateOfCreationAscending} value={5} />
      <OptionComponent {...optionSalaryFromDescending} value={6} />
      <OptionComponent {...optionSalaryFromAscending} value={1} />
      <OptionComponent {...optionSalaryToDescending} value={2} />
      <OptionComponent {...optionSalaryToAscending} value={3} />
    </SelectComponent>
  );
};
