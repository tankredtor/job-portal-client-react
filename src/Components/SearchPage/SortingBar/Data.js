export const selectSortingObj = {
  htmlBlock: "search-block__sorting",
  name: "searchInput",
  type: "number",
};

export const optionSalaryFromDescending = {
  description: "По убыв. мин. дохода",
  type: "number",
};

export const optionSalaryFromAscending = {
  description: "По возр. мин. дохода",
  type: "number",
};
export const optionSalaryToDescending = {
  description: "По убыв. макс. дохода",
  type: "number",
};
export const optionSalaryToAscending = {
  description: "По возр. макс. дохода",
  type: "number",
};
export const optionDateOfCreationDescending = {
  description: "Вначале новые",
  type: "number",
};
export const optionDateOfCreationAscending = {
  description: "Вначале старые",
  type: "number",
};
