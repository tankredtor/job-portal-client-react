import React from "react";

export const NoVacancyFilter = () => {
  return (
    <div className="no-vacancy-filter">
      Нет вакансий удовлетворяющих ваш запрос
    </div>
  );
};
