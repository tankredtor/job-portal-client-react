import React from "react";
import { NoVacancy } from "./NoVacancy";
import { NoVacancyFilter } from "./NoVacancyFilter";
import { SearchListComponent } from "../List/SearchListComponent";
import { vacancySearchData } from "../List/Card/Data";

export const VacanciesBlockSearchShow = ({ vacancies }) => {
  if (vacancies.length === 0) return <NoVacancyFilter />;
  else if (vacancies.length !== 0)
    return <SearchListComponent {...vacancySearchData} dataList={vacancies} />;
  return <NoVacancy />;
};
