import React, { useCallback } from "react";
import { ButtonStandartComponent } from "../../Buttons/ButtonStandartComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import {
  buttonRight,
  buttonLeft,
  spanfirstPageObj,
  spanLastPageObj,
  spanSinglePageObj,
} from "./Data";

export const PaginationButtonsComponent = ({
  page,
  pages,
  paginationSwitchHandler,
}) => {
  const pageButtonPrev = useCallback(
    (e) => {
      e.preventDefault();

      paginationSwitchHandler(page - 1);
    },
    [paginationSwitchHandler, page]
  );

  const pageButtonNext = useCallback(
    (e) => {
      e.preventDefault();
      paginationSwitchHandler(page + 1);
    },
    [paginationSwitchHandler, page]
  );

  if (page === 1 && pages > 1) {
    return (
      <>
        <SpanComponent {...spanfirstPageObj} />
        <ButtonStandartComponent {...buttonRight} onClick={pageButtonNext} />
      </>
    );
  } else if (page < pages && page !== 0) {
    return (
      <>
        <ButtonStandartComponent {...buttonLeft} onClick={pageButtonPrev} />
        <ButtonStandartComponent {...buttonRight} onClick={pageButtonNext} />
      </>
    );
  } else if (page === pages && pages > 1) {
    return (
      <>
        <ButtonStandartComponent {...buttonLeft} onClick={pageButtonPrev} />
        <SpanComponent {...spanLastPageObj} />
      </>
    );
  }
  return <SpanComponent {...spanSinglePageObj} />;
};
