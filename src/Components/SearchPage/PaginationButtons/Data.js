export const buttonLeft = {
  htmlName: "left-page",
  description: "Предыдущая",
};

export const buttonRight = {
  htmlName: "right-page",
  description: "Следующая",
};

export const spanfirstPageObj = {
  htmlBlock: "disable-button",
  typography: "heading-sub-information",
  body: "Первая страница",
};

export const spanLastPageObj = {
  htmlBlock: "disable-button",
  typography: "heading-sub-information",
  body: "Последняя страница",
};

export const spanSinglePageObj = {
  htmlBlock: "disable-button",
  typography: "heading-sub-information",
  body: "Одна страница",
};
