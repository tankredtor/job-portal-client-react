export const selectWrapperObj = {
  htmlBlock: "filter-select-experience",
};

export const spanSelectExperienceObj = {
  htmlBlock: "filter-span",
  body: "Требуемый опыт работы",
  typography: "heading-sub-information",
};

export const selectFilterObj = {
  name: "sortingFieldName",
  htmlBlock: "filter-select-experience__select",
  type: "number",
};

export const optionFilterAllObj = {
  description: "Все",
  htmlBlock: "filter-select-experience__option",
};

export const optionFilterNoExperinceObj = {
  description: "Без опыта работы",
  htmlBlock: "filter-select-experience__option",
};
export const optionFilterFromOneToThreeObj = {
  description: "От 1 года до 3 лет",
  htmlBlock: "filter-select-experience__option",
};
export const optionFilterFromThreeToSixObj = {
  description: "От 3 лет до 6 лет",
  htmlBlock: "filter-select-experience__option",
};
export const optionFilterFromSixObj = {
  description: "Более 6 лет",
  htmlBlock: "filter-select-experience__option",
};

export const salaryWrapperObj = {
  htmlBlock: "filter-salary",
};
export const spanInputSalatyFromObj = {
  htmlBlock: "filter-span",
  body: "Доход от",
  typography: "heading-sub-information",
};

export const inputSalaryFromObj = {
  type: "number",
  placeholder: "0",
  name: "filteringSalaryFrom",
  htmlBlock: "filter-salary__input",
};

export const spanInputSalatyToObj = {
  htmlBlock: "filter-span",
  body: "Доход до",
  typography: "heading-sub-information",
};

export const inputSalaryToObj = {
  type: "number",
  placeholder: "0",
  name: "filteringSalaryTo",
  htmlBlock: "filter-salary__input",
};

export const buttonBubmitObj = {
  description: "Применить фильтр",
  htmlName: "filter-submit",
};

export const reloadWhrapperObj = {
  htmlBlock: "reload-whrapper",
};

export const reloadFilterObj = {
  htmlBlock: "reload-filter",
  typography: "heading-sub-information",
  body: "Сбросить фильтр",
};
