import React from "react";
import { SelectComponent } from "../../ReusableComponents/ActiveComponents/SelectComponent";
import { OptionComponent } from "../../ReusableComponents/ActiveComponents/OptionComponent";
import { InputComponent } from "../../ReusableComponents/ActiveComponents/InputComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { ButtonStandartComponent } from "../../Buttons/ButtonStandartComponent";
import { AiOutlineClose } from "react-icons/ai";
import {
  selectWrapperObj,
  spanSelectExperienceObj,
  selectFilterObj,
  optionFilterAllObj,
  optionFilterNoExperinceObj,
  optionFilterFromOneToThreeObj,
  optionFilterFromThreeToSixObj,
  optionFilterFromSixObj,
  salaryWrapperObj,
  spanInputSalatyFromObj,
  inputSalaryFromObj,
  spanInputSalatyToObj,
  inputSalaryToObj,
  buttonBubmitObj,
  reloadFilterObj,
  reloadWhrapperObj,
} from "./Data";
export const FilterSearchComponent = ({
  valueSalaryFrom,
  salaryFromChangeFunc,
  valueSalaryTo,
  salaryToChangeFunc,
  valueFilteringRequiredExperience,
  setFilteringRequiredExperience,
  searchHandler,
  setReloadFilter,
  showFilter,
  setSorting,
}) => {
  const filteringRequiredExperienceChangeHandler = (e) => {
    if (e.target.value >= 0) {
      setFilteringRequiredExperience(parseInt(e.target.value, 10));
    } else {
      setFilteringRequiredExperience(undefined);
    }
  };

  return (
    <>
      <BlockComponent {...reloadWhrapperObj}>
        <AiOutlineClose
          className="close-filter"
          size="30"
          onClick={showFilter}
        />
        <SpanComponent {...reloadFilterObj} onClick={setReloadFilter} />
      </BlockComponent>

      <BlockComponent {...selectWrapperObj}>
        <SpanComponent {...spanSelectExperienceObj} />
        <SelectComponent
          {...selectFilterObj}
          value={valueFilteringRequiredExperience || undefined}
          onChange={filteringRequiredExperienceChangeHandler}
        >
          <OptionComponent {...optionFilterAllObj} value={undefined} />
          <OptionComponent {...optionFilterNoExperinceObj} value={0} />
          <OptionComponent {...optionFilterFromOneToThreeObj} value={1} />
          <OptionComponent {...optionFilterFromThreeToSixObj} value={2} />
          <OptionComponent {...optionFilterFromSixObj} value={3} />
        </SelectComponent>
      </BlockComponent>
      <BlockComponent {...salaryWrapperObj}>
        <SpanComponent {...spanInputSalatyFromObj} />
        <InputComponent
          {...inputSalaryFromObj}
          value={valueSalaryFrom || ""}
          onChange={(e) => salaryFromChangeFunc(e.target.value)}
        />
      </BlockComponent>

      <BlockComponent {...salaryWrapperObj}>
        <SpanComponent {...spanInputSalatyToObj} />
        <InputComponent
          {...inputSalaryToObj}
          value={valueSalaryTo || ""}
          onChange={(e) => salaryToChangeFunc(e.target.value)}
        />
      </BlockComponent>
      <ButtonStandartComponent {...buttonBubmitObj} onClick={searchHandler} />
    </>
  );
};
