import React from "react";
import { NoResume } from "./NoResume";
import { NoResumeFilter } from "./NoResumeFilter";
import { SearchListComponent } from "../List/SearchListComponent";
import { resumeSearchData } from "../List/Card/Data";

export const ResumesBlockSearchShow = ({ resumes }) => {
  if (resumes.length === 0) return <NoResumeFilter />;
  else if (resumes.length !== 0)
    return <SearchListComponent {...resumeSearchData} dataList={resumes} />;
  return <NoResume />;
};
