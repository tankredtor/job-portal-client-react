import React from "react";

export const NoResumeFilter = () => {
  return (
    <div className="no-resume-filter">
      Нет резюме удовлетворяющих ваш запрос
    </div>
  );
};
