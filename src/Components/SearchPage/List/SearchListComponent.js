import React from "react";
import { SearchCardComponent } from "./Card/SearchCardComponent";

export const SearchListComponent = ({
  dataList,
  urlCard,
  urlOwner,
  whatDataUpdatedAt,
}) => {
  const renderList = dataList.map((data) => {
    return (
      <SearchCardComponent
        key={data.id}
        data={data}
        urlCard={urlCard}
        urlOwner={urlOwner}
        whatDataUpdatedAt={whatDataUpdatedAt}
      />
    );
  });

  return (
    <div className="vacancies-list">
      <div className="vacancies-list__scroll">{renderList}</div>
    </div>
  );
};
