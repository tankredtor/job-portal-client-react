import React from "react";

export const SalaryElement = ({ data }) => {
  if (data.salaryFrom && data.salaryTo) {
    return (
      <div className="search-card__salary heading-sub-information">
        {`${data.salaryFrom} -  ${data.salaryTo} Руб. `}
      </div>
    );
  } else if (data.salaryFrom && !data.salaryTo) {
    return (
      <div className="search-card__salary heading-sub-information">
        {`от ${data.salaryFrom} Руб.`}
      </div>
    );
  } else if (data.salaryTo && !data.salaryFrom) {
    return (
      <div className="search-card__salary heading-sub-information">
        {` до ${data.salaryTo} Руб.`}
      </div>
    );
  } else if (data.salaryTo === 0 && data.salaryTo === data.salaryFrom) {
    return (
      <div className="search-card__salary heading-sub-information">
        Доход не указан
      </div>
    );
  } else
    return (
      <div className="search-card__salary heading-sub-information">
        Доход не указан
      </div>
    );
};
