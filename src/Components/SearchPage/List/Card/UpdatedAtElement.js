import React from "react";

export const UpdatedAtElement = ({ data, whatDataUpdatedAt }) => {
  const dateOfCreation = (date) => {
    let today = new Date(date);
    const dd = String(today.getDate()).padStart(2, "0");
    const mm = String(today.getMonth() + 1).padStart(2, "0");
    const yyyy = today.getFullYear();

    today = dd + "." + mm + "." + yyyy;
    return today;
  };

  return (
    <div className="search-card__updated-at heading-grey">
      {`${whatDataUpdatedAt} ${dateOfCreation(data.updatedAt)}`}
    </div>
  );
};
