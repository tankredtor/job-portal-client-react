import React from "react";
import { Link } from "react-router-dom";
import { SalaryElement } from "./SalaryElement";
import { OwnerElemet } from "./OwnerElement";
import { UpdatedAtElement } from "./UpdatedAtElement";

export const SearchCardComponent = ({
  data,
  urlCard,
  urlOwner,
  whatDataUpdatedAt,
}) => {
  const limitTitle = (title, limit = 20) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split(" ").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join(" ")}...`;
    }
    return title;
  };

  return (
    <>
      <div className="search-card">
        <Link
          className="search-card__link"
          style={{ textDecoration: "none" }}
          to={`${urlCard + data.id}`}
        >
          <div className="search-card__name heading-main-information">
            {limitTitle(data.name)}
          </div>
          {data.createdAt && (
            <UpdatedAtElement
              whatDataUpdatedAt={whatDataUpdatedAt}
              data={data}
            />
          )}

          <SalaryElement data={data} />
        </Link>
        <OwnerElemet data={data} urlOwner={urlOwner} />
      </div>
    </>
  );
};
