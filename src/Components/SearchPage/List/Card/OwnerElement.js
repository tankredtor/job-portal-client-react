import React from "react";
import { Link } from "react-router-dom";

export const OwnerElemet = ({ data, urlOwner }) => {
  const limitCharacters = (title, limit = 26) => {
    const newTitle = [];
    if (title.length > limit) {
      title.split("").reduce((acc, cur) => {
        if (acc + cur.length <= limit) {
          newTitle.push(cur);
        }
        return acc + cur.length;
      }, 0);
      return `${newTitle.join("")}...`;
    }
    return title;
  };

  if (data.employer) {
    return (
      <Link
        style={{ textDecoration: "none" }}
        to={`${urlOwner + data.employer.id}`}
        className="search-card__owner heading-sub-information "
      >
        {limitCharacters(data.employer.name)}
      </Link>
    );
  }

  return (
    <Link
      style={{ textDecoration: "none" }}
      to={`${urlOwner + data.applicant.id}`}
      className="search-card__owner heading-sub-information "
    >
      {`${limitCharacters(data.applicant.lastName)} ${limitCharacters(
        data.applicant.firstName
      )}`}
    </Link>
  );
};
