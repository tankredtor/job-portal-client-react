export const vacancySearchData = {
  urlCard: "/detail-vacancy/",
  urlOwner: "/detail-employer/",
  whatDataUpdatedAt: "Вакансия создана",
};

export const resumeSearchData = {
  urlCard: "/detail-resume/",
  urlOwner: "/detail-applicant/",
  whatDataUpdatedAt: "Резюме создано",
};
