import React from "react";

export const RadioElement = ({
  value,
  name,
  label,
  onChange,
  cheked,
  typography,
}) => {
  return (
    <label className="radio-form__radio">
      <input
        name={name}
        className="radio-form__radio__input"
        type="radio"
        value={value}
        onChange={onChange}
        checked={cheked}
      />
      <span className={`radio-form__radio__label heading-${typography}`}>
        {label}
      </span>
    </label>
  );
};
