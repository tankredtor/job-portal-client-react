import React from "react";

export const FormComponent = ({ children, onKeyPress, htmlTag }) => {
  return (
    <form onKeyPress={onKeyPress} className={`form-component form-${htmlTag}`}>
      {children}
    </form>
  );
};
