import React from "react";

export const InputElement = ({
  heading,
  type,
  placeholder,
  name,
  value,
  onChange,
  size,
  htmlTag,
  children,
  typography,
  onKeyPress,
  autocomplete,
}) => {
  return (
    <div className={`input-element form-${htmlTag}__input`}>
      <span
        className={`input-element__label form-${htmlTag}__input__label heading-${typography}`}
      >
        {heading}
      </span>
      <input
        placeholder={placeholder}
        id={name}
        autoComplete={autocomplete}
        type={type}
        name={name}
        onKeyPress={onKeyPress}
        className={`input-element__input-field-${size} form-${htmlTag}__input__input-field `}
        value={value}
        onChange={onChange}
      />
      {children}
    </div>
  );
};
