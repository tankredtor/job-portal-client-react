import React from "react";

export const RadioFormComponent = ({ children }) => {
  return (
    <div className="radio-form " action="#">
      {children}
    </div>
  );
};
