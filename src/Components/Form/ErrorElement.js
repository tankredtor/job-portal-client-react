import React from "react";

export const ErrorElement = ({ error, htmlTag }) => {
  return (
    <span
      className={`error-element form-${htmlTag}__input__error heading-sub-information`}
    >
      {error}
    </span>
  );
};
