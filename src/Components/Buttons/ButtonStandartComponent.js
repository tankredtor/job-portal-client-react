import React from "react";

export const ButtonStandartComponent = ({
  onClick,
  description,
  htmlName,
  value,
  children,
}) => {
  return (
    <button
      className={`${htmlName}-button standart-button  `}
      onClick={onClick}
      value={value}
    >
      {description}
      {children}
    </button>
  );
};
