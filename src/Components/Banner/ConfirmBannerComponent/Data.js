export const bannerBlockObj = {
  htmlBlock: "confirm-banner",
};

export const spanObj = {
  htmlBlock: "confirm-banner__span",
  body: "Вы уверены, что хотите совершить это действие?",
  typography: "heading-sub-information",
};
