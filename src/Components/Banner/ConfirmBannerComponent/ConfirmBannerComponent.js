import React from "react";
import { BlockComponent } from "../../BlockComponent/BlockComponent";
import { SpanComponent } from "../../ReusableComponents/PresenatationComponents/SpanComponent";
import { FaCheck } from "react-icons/fa";
import { ImCross } from "react-icons/im";
import { spanObj, bannerBlockObj } from "./Data";

export const ConfirmBannerComponent = ({ confirm, refusal }) => {
  return (
    <BlockComponent {...bannerBlockObj}>
      <SpanComponent {...spanObj} />
      <FaCheck
        size="30"
        className="confirm-banner__check-icon"
        onClick={confirm}
      />
      <ImCross
        size="30"
        className="confirm-banner__cross-icon"
        onClick={refusal}
      />
    </BlockComponent>
  );
};
