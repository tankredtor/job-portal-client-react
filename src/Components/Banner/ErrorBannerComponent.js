import React from "react";

export const ErrorBannerComponent = ({ heading, body, children }) => {
  return (
    <div className="banner-whrapper">
      <div className="error-banner">
        <div className="error-banner__heading">{heading}</div>
        <div className="error-banner__body">{body}</div>
        {children}
      </div>
    </div>
  );
};
