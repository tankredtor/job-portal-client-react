import React, { useContext, useState, useEffect } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";
import { SpanComponent } from "../ReusableComponents/PresenatationComponents/SpanComponent";
import { ProjectLogo } from "../ProjectLogo/ProjectLogo";
import { BlockComponent } from "../BlockComponent/BlockComponent";

import {
  spanProjectLogo1Obj,
  spanWrapperLogoObj,
  projectLogoImgObj,
  projectLogoObj,
  spanProjectLogo2Obj,
} from "./Data";

export const Navbar = () => {
  const history = useHistory();
  const auth = useContext(AuthContext);
  const [header, setHeader] = useState("header");

  const logoutHandler = (event) => {
    event.preventDefault();
    auth.logout();
    history.push("/");
  };

  const listenScrollEvent = () => {
    if (window.scrollY < 2) {
      return setHeader("navbar-glass");
    } else if (window.scrollY > 4) {
      return setHeader("navbar-grey");
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", listenScrollEvent);

    return () => {
      window.removeEventListener("scroll", listenScrollEvent);
    };
  });

  return (
    <div className={`navbar ${header}`}>
      <NavLink
        className="nav-button nav-button__my-page"
        activeClassName="active-nav-button"
        to="/my-page"
      >
        Моя страница
      </NavLink>

      <NavLink
        className="nav-button nav-button__vacancies "
        activeClassName="active-nav-button"
        to="/vacancies"
      >
        Вакансии
      </NavLink>

      <NavLink
        className="nav-button nav-button__resumes nav-button__resume"
        activeClassName="active-nav-button"
        to="/resumes"
      >
        Резюме
      </NavLink>

      <BlockComponent {...projectLogoObj}>
        <ProjectLogo {...projectLogoImgObj} />
        <BlockComponent {...spanWrapperLogoObj}>
          <SpanComponent {...spanProjectLogo1Obj} />
          <SpanComponent {...spanProjectLogo2Obj} />
        </BlockComponent>
      </BlockComponent>

      <a
        className="nav-button nav-button__logout "
        href="/"
        onClick={logoutHandler}
      >
        Выход
      </a>
    </div>
  );
};
