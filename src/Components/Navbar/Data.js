export const projectLogoObj = {
  htmlBlock: "navbar__project-logo ",
};

export const projectLogoImgObj = {
  htmlBlock: "navbar__project-logo__img",
};

export const spanWrapperLogoObj = {
  htmlBlock: "navbar__project-logo__span-whrapper",
};

export const spanProjectLogo1Obj = {
  htmlBlock: "navbar__project-logo__span-main",
  body: "Карьерный",
  typography: "heading-logo-navbar",
};

export const spanProjectLogo2Obj = {
  htmlBlock: "navbar__project-logo__span-main",
  body: "Центр",
  typography: "heading-logo-navbar",
};
