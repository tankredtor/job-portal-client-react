import React, { useContext } from "react";
import { NavLink, useHistory } from "react-router-dom";
import { AuthContext } from "../../context/AuthContext";
import { SpanComponent } from "../ReusableComponents/PresenatationComponents/SpanComponent";
import { ProjectLogo } from "../ProjectLogo/ProjectLogo";
import { BlockComponent } from "../BlockComponent/BlockComponent";
import { ImExit } from "react-icons/im";
import { MdAccountBalance } from "react-icons/md";
import { FaHome, FaUserGraduate } from "react-icons/fa";

import {
  spanProjectLogo1Obj,
  spanWrapperLogoObj,
  projectLogoImgObj,
  projectLogoObj,
  spanProjectLogo2Obj,
} from "./Data";

export const NavbarMobile = () => {
  const history = useHistory();
  const auth = useContext(AuthContext);

  const logoutHandler = (event) => {
    event.preventDefault();
    auth.logout();
    history.push("/");
  };

  return (
    <div className="navbar-mobile">
      <NavLink
        className="navbar-mobile__button"
        activeClassName="navbar-mobile__button navbar-mobile__button__active"
        to="/my-page"
      >
        <FaHome className="navbar-mobile__button__icon" size="30" />
      </NavLink>

      <NavLink
        className="navbar-mobile__button"
        activeClassName="navbar-mobile__button navbar-mobile__button__active"
        to="/vacancies"
      >
        <MdAccountBalance className="navbar-mobile__button__icon" size="30" />
      </NavLink>

      <NavLink
        className="navbar-mobile__button"
        activeClassName="navbar-mobile__button navbar-mobile__button__active"
        to="/resumes"
      >
        <FaUserGraduate className="navbar-mobile__button__icon" size="30" />
      </NavLink>

      <BlockComponent {...projectLogoObj}>
        <ProjectLogo {...projectLogoImgObj} />
        <BlockComponent {...spanWrapperLogoObj}>
          <SpanComponent {...spanProjectLogo1Obj} />
          <SpanComponent {...spanProjectLogo2Obj} />
        </BlockComponent>
      </BlockComponent>

      <a
        className="navbar-mobile__button navbar-mobile__button__logout "
        href="/"
        onClick={logoutHandler}
      >
        <ImExit className="navbar-mobile__button__icon" size="30" />
      </a>
    </div>
  );
};
