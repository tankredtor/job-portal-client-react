import React from "react";
import { BlockComponent } from "../BlockComponent/BlockComponent";
import { SpanComponent } from "../ReusableComponents/PresenatationComponents/SpanComponent";
import { ProjectLogo } from "../ProjectLogo/ProjectLogo";
import { Icon24LogoVk } from "@vkontakte/icons";
import { FaFacebookSquare } from "react-icons/fa";
import { AiFillInstagram } from "react-icons/ai";
import { Link } from "react-router-dom";

import {
  whrapperObj,
  socialIconsObj,
  webRightsObj,
  projectLogoObj,
  spanWebRightsObj,
  projectLogoImgObj,
  spanProjectLogo1Obj,
  spanProjectLogo2Obj,
  spanProjectLogo3Obj,
  spanWrapperLogoObj,
} from "./Data";
export const Footer = () => {
  const spanCurrentYear = () => {
    return `СОДРУЖЕСТВО ВЫПУСКНИКОВ МГЮА ©${new Date().getFullYear()}`;
  };

  return (
    <BlockComponent {...whrapperObj}>
      <BlockComponent {...projectLogoObj}>
        <ProjectLogo {...projectLogoImgObj} />
        <BlockComponent {...spanWrapperLogoObj}>
          <SpanComponent {...spanProjectLogo1Obj} />
          <SpanComponent {...spanProjectLogo2Obj} />
          <SpanComponent {...spanProjectLogo3Obj} />
        </BlockComponent>
      </BlockComponent>
      <BlockComponent {...webRightsObj}>
        <SpanComponent {...spanWebRightsObj} body={spanCurrentYear()} />
      </BlockComponent>
      <BlockComponent {...socialIconsObj}>
        <Link target="_blank" to={{ pathname: "https://vk.com/msal.alumni" }}>
          <Icon24LogoVk
            className="footer__social-icons__vk footer__social-icons__icon"
            width={60}
            height={60}
          />
        </Link>

        <Link
          className="footer__social-icons__facebook-a-tag"
          target="_blank"
          to={{ pathname: "https://www.facebook.com/msal.alumni" }}
        >
          <FaFacebookSquare
            className="footer__social-icons__facebook footer__social-icons__icon"
            size="51"
          />
        </Link>
        <Link
          className="footer__social-icons__facebook-a-tag"
          target="_blank"
          to={{ pathname: "https://www.instagram.com/msal_alumni/" }}
        >
          <AiFillInstagram
            className="footer__social-icons__instagram footer__social-icons__icon"
            size="60"
          />
        </Link>
      </BlockComponent>
    </BlockComponent>
  );
};
