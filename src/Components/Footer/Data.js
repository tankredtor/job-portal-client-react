export const whrapperObj = {
  htmlBlock: "footer",
};

export const projectLogoObj = {
  htmlBlock: "footer__project-logo ",
};
export const webRightsObj = {
  htmlBlock: "footer__web-rights",
};
export const socialIconsObj = {
  htmlBlock: "footer__social-icons",
};

export const spanWebRightsObj = {
  htmlBlock: "footer__web-rights__span",
  typography: "heading-web-rights",
};

export const projectLogoImgObj = {
  htmlBlock: "footer__project-logo__img",
};

export const spanWrapperLogoObj = {
  htmlBlock: "footer__project-logo__span-whrapper",
};

export const spanProjectLogo1Obj = {
  htmlBlock: "footer__project-logo__span-main",
  body: "Карьерный",
  typography: "heading-logo-footer",
};

export const spanProjectLogo2Obj = {
  htmlBlock: "footer__project-logo__span-main",
  body: "Центр",
  typography: "heading-logo-footer",
};

export const spanProjectLogo3Obj = {
  htmlBlock: "footer__project-logo__span-sub",
  body: "Содружества выпускников МГЮА",
  typography: "heading-sub-logo-footer",
};
