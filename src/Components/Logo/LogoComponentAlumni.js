import React from "react";
import LogoAlumni from "../../styles/images/Logo/alumniLogo.png";
export const LogoComponentAlumni = ({ htmlName, src }) => {
  return (
    <div className={`${htmlName}-page__logo-box`}>
      <img
        src={LogoAlumni}
        alt="Logo"
        className={`${htmlName}-page__logo-box__logo`}
      />
    </div>
  );
};
