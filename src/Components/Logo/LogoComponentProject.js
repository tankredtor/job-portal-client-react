import React from "react";

export const LogoComponentProject = ({ htmlName }) => {
  return (
    <div className={`${htmlName}-page__text-box`}>
      <h1 className="heading-primary__main heading-primary__main__upper">
        Карьерный
      </h1>
      <h1 className="heading-primary__main heading-primary__main__upper">
        центр
      </h1>
      <h1 className="heading-primary__sub heading-primary__sub__down">
        Содружества выпускников МГЮА
      </h1>
    </div>
  );
};
