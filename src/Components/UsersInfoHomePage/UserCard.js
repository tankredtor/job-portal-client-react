import React from "react";

export const UserCard = ({
  htmlTagList,
  htmlTagHeading = "",
  htmlBlock = "",
  htmlElement = "",
  htmlElementPointer = "",
  headingText,
  elementText1,
  elementText2,
  elementText3,
  elementPointer1 = "",
  elementPointer2 = "",
  elementPointer3 = "",
  children,
}) => {
  return (
    <div className={`${htmlBlock}`}>
      <div className={`${htmlBlock}${htmlTagHeading}`}> {headingText} </div>
      {children}
      <div className={`${htmlBlock}${htmlTagList}`}>
        <span className={`${htmlBlock}${htmlTagList}${htmlElementPointer}`}>
          {elementPointer1}
        </span>
        <span className={`${htmlBlock}${htmlTagList}${htmlElement}`}>
          {elementText1}
        </span>
        <span className={`${htmlBlock}${htmlTagList}${htmlElementPointer}`}>
          {elementPointer2}
        </span>
        <span className={`${htmlBlock}${htmlTagList}${htmlElement}`}>
          {elementText2}
        </span>
        <span className={`${htmlBlock}${htmlTagList}${htmlElementPointer}`}>
          {elementPointer3}
        </span>
        <span className={`${htmlBlock}${htmlTagList}${htmlElement}`}>
          {elementText3}
        </span>
      </div>
    </div>
  );
};
