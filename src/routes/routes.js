import React from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import { HomePage } from "../pages/HomePage/HomePage";
import { MyPageEmployer } from "../pages/MyPage/MyPageEmployer/MyPageEmployer";
import { CreateEmployerPage } from "../pages/CreateEmployerPage/CreateEmployerPage";
import { DetailEmployerPage } from "../pages/DetailUserPage/DetailEmployerPage/DetailEmployerPage";
import { ChangeEmployerPage } from "../pages/ChangeEmployerPage/ChangeEmployerPage";
import { CreateVacancyPage } from "../pages/CreacteVacancyPage/CreateVacancyPage";
import { DetailVacancyPage } from "../pages/DetailCardPage/DetailVacancyPage/DetailVacancyPage";
import { ChangeVacancyPage } from "../pages/ChangeVacancyPage/ChangeVacancyPage";
import { MyPageApplicant } from "../pages/MyPage/MyPageApplicant/MyPageApplicant";
import { CreateApplicantPage } from "../pages/CreateApplicantPage/CreateApplicantPage";
import { DetailApplicantPage } from "../pages/DetailUserPage/DetailApplicantPage/DetailApplicantPage";
import { ChangeApplicantPage } from "../pages/ChangeApplicantPage/ChangeApplicantPage";
import { CreateResumePage } from "../pages/CreateResumePage/CreateResumePage";
import { DetailResumePage } from "../pages/DetailCardPage/DetailResumePage/DetailResumePage";
import { ChangeResumePage } from "../pages/ChangeResumePage/ChangeResumePage";
import { SearchPageResume } from "../pages/SearchPageResume/SearchPageResume";
import { SearchPageVacancy } from "../pages/SearchPageVacancy/SearchPageVacancy";
import { PrivacyPolicyPage } from "../pages/PrivacyPolicyPage/PrivacyPolivyPage";
export const useRoutes = (isAuthenticated, userRole) => {
  if (isAuthenticated && userRole === "Employer") {
    return (
      <Switch>
        <Route path="/resumes" exact>
          <SearchPageResume />
        </Route>
        <Route path="/vacancies" exact>
          <SearchPageVacancy />
        </Route>
        <Route path="/my-page" exact>
          <MyPageEmployer />
        </Route>
        <Route path="/create-employer" exact>
          <CreateEmployerPage />
        </Route>
        <Route path="/detail-vacancy/:id">
          <DetailVacancyPage />
        </Route>
        <Route path="/change-vacancy/:id">
          <ChangeVacancyPage />
        </Route>
        <Route path="/create-vacancy" exact>
          <CreateVacancyPage />
        </Route>
        <Route path="/detail-resume/:id">
          <DetailResumePage />
        </Route>
        <Route path="/detail-employer/:id">
          <DetailEmployerPage />
        </Route>
        <Route path="/change-employer/:id">
          <ChangeEmployerPage />
        </Route>
        <Route path="/detail-applicant/:id">
          <DetailApplicantPage />
        </Route>
        <Route path="/privacy" exact>
          <PrivacyPolicyPage />
        </Route>
        <Redirect to="/resumes" />
      </Switch>
    );
  } else if (isAuthenticated && userRole === "Applicant") {
    return (
      <Switch>
        <Route path="/my-page" exact>
          <MyPageApplicant />
        </Route>
        <Route path="/resumes" exact>
          <SearchPageResume />
        </Route>
        <Route path="/vacancies" exact>
          <SearchPageVacancy />
        </Route>
        <Route path="/detail-vacancy/:id">
          <DetailVacancyPage />
        </Route>
        <Route path="/create-applicant" exact>
          <CreateApplicantPage />
        </Route>
        <Route path="/change-applicant/:id" exact>
          <ChangeApplicantPage />
        </Route>
        <Route path="/detail-resume/:id">
          <DetailResumePage />
        </Route>
        <Route path="/detail-employer/:id">
          <DetailEmployerPage />
        </Route>
        <Route path="/change-resume/:id">
          <ChangeResumePage />
        </Route>
        <Route path="/create-resume" exact>
          <CreateResumePage />
        </Route>
        <Route path="/detail-applicant/:id">
          <DetailApplicantPage />
        </Route>
        <Route path="/privacy" exact>
          <PrivacyPolicyPage />
        </Route>
        <Redirect to="/vacancies" />
      </Switch>
    );
  } else if (isAuthenticated && userRole === "Admin") {
    return (
      <Switch>
        <Route path="/my-page" exact>
          <MyPageApplicant />
        </Route>
        <Route path="/resumes" exact>
          <SearchPageResume />
        </Route>
        <Route path="/change-applicant/:id" exact>
          <ChangeApplicantPage />
        </Route>
        <Route path="/change-resume/:id">
          <ChangeResumePage />
        </Route>
        <Route path="/vacancies" exact>
          <SearchPageVacancy />
        </Route>
        <Route path="/detail-vacancy/:id">
          <DetailVacancyPage />
        </Route>
        <Route path="/change-vacancy/:id">
          <ChangeVacancyPage />
        </Route>
        <Route path="/create-applicant" exact>
          <CreateApplicantPage />
        </Route>
        <Route path="/change-applicant/:id" exact>
          <ChangeApplicantPage />
        </Route>
        <Route path="/detail-resume/:id">
          <DetailResumePage />
        </Route>
        <Route path="/detail-employer/:id">
          <DetailEmployerPage />
        </Route>
        <Route path="/change-employer/:id">
          <ChangeEmployerPage />
        </Route>
        <Route path="/change-resume/:id">
          <ChangeResumePage />
        </Route>
        <Route path="/create-resume" exact>
          <CreateResumePage />
        </Route>
        <Route path="/detail-applicant/:id">
          <DetailApplicantPage />
        </Route>
        <Route path="/privacy" exact>
          <PrivacyPolicyPage />
        </Route>
        <Redirect to="/vacancies" />
      </Switch>
    );
  }

  return (
    <Switch>
      <Route path="/" exact>
        <HomePage />
      </Route>
      <Route path="/privacy" exact>
        <PrivacyPolicyPage />
      </Route>
      <Redirect to="/" />
    </Switch>
  );
};
