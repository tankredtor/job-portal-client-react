import { createContext } from "react";

function noop() {}

export const AuthContext = createContext({
  token: null,
  username: null,
  login: noop,
  userRole: null,
  logout: noop,
  isAuthenticated: false,
  userId: null,
});
