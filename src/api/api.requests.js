import { useCallback, useContext, useEffect, useMemo, useState } from 'react';
import { AuthContext } from '../context/AuthContext';
import api from './api';

export const ApiRequests = () => {
  const { token } = useContext(AuthContext);
  const [loading, setLoading] = useState(false);
  const { logout } = useContext(AuthContext);
  const [errorSetter, setErrorSetter] = useState(null);

  useEffect(() => {
    if (errorSetter && errorSetter.status === 401) {
      logout();
    }
    return () => {
      setErrorSetter(null);
    };
  }, [errorSetter, logout]);

  const auth = useMemo(() => {
    return {
      Authorization: `Bearer ${token}`,
    };
  }, [token]);

  const postApiAuth = useCallback(async (data, url) => {
    setLoading(true);
    try {
      const request = await api.post(url, data);
      setLoading(false);
      console.log(request);
      return request;
    } catch (e) {
      setLoading(false);
      console.log(e);
      console.log(e.request);
      console.log(e.response);
      console.log(e.message);
      return e.response;
    }
  }, []);

  const getApiData = useCallback(
    async (setter, url) => {
      setLoading(true);

      try {
        const fetched = await api.get(url, { headers: auth });
        setLoading(false);
        setter(fetched.data);
      } catch (e) {
        setLoading(false);
        setErrorSetter(e.response);
      }
    },
    [auth, setErrorSetter]
  );

  const getApiDataFilterVacancy = useCallback(
    async (setter, setterPage, setterPages, url, params) => {
      setLoading(true);
      try {
        const fetched = await api.get(url, { params, headers: auth });
        setLoading(false);
        setter(fetched.data.vacancies);
        setterPage(fetched.data.page);
        setterPages(fetched.data.pages);
      } catch (e) {
        setLoading(false);
        setErrorSetter(e.response);
      }
    },
    [auth, setErrorSetter]
  );

  const getApiDataFilterResumes = useCallback(
    async (setter, setterPage, setterPages, url, params) => {
      setLoading(true);
      try {
        const fetched = await api.get(url, { params, headers: auth });
        setLoading(false);
        setter(fetched.data.resumes);
        setterPage(fetched.data.page);
        setterPages(fetched.data.pages);
      } catch (e) {
        setLoading(false);
        setErrorSetter(e.response);
      }
    },
    [auth, setErrorSetter]
  );

  const postApiData = useCallback(
    async (data, url) => {
      setLoading(true);
      try {
        const dataPost = await api.post(url, data, { headers: auth });
        setLoading(false);
        return dataPost;
      } catch (e) {
        setLoading(false);
        return e.response;
      }
    },
    [auth]
  );

  const putApiData = useCallback(
    async (data, url) => {
      setLoading(true);
      try {
        const dataPost = await api.put(url, data, { headers: auth });
        setLoading(false);
        console.log(dataPost);
      } catch (e) {
        setLoading(false);
        setErrorSetter(e.response);
      }
    },
    [auth, setErrorSetter]
  );

  const deleteApiData = useCallback(
    async (url) => {
      setLoading(true);
      try {
        const dataDelete = await api.delete(url, { headers: auth });
        setLoading(false);
        console.log(dataDelete);
      } catch (e) {
        setLoading(false);
        setErrorSetter(e.response);
      }
    },
    [auth, setErrorSetter]
  );

  return {
    postApiAuth,
    getApiData,
    getApiDataFilterVacancy,
    getApiDataFilterResumes,
    postApiData,
    deleteApiData,
    putApiData,
    loading,
  };
};
