import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import { useRoutes } from "./routes/routes";
import { useAuth } from "./hooks/auth.hook";
import { useEmployerInfo } from "./hooks/employer.hook";
import { AuthContext } from "./context/AuthContext";
import { Navbar } from "./Components/Navbar/Navbar";
import { Footer } from "./Components/Footer/Footer";
import { NavbarMobile } from "./Components/Navbar/NavbarMobile";
import Loader from "./styles/images/Loader/Spinner-1s-200px.svg";
import "materialize-css";
import "./App.scss";
import "./index.css";

function App() {
  const { token, login, logout, username, ready, userRole, userId } = useAuth();
  const { getEmployerId, employerId } = useEmployerInfo();

  const isAuthenticated = !!token;
  const routes = useRoutes(isAuthenticated, userRole);
  if (!ready) {
    return <Loader />;
  }

  return (
    <AuthContext.Provider
      value={{
        token,
        login,
        logout,
        username,
        isAuthenticated,
        userRole,
        userId,
        employerId,
        getEmployerId,
      }}
    >
      <Router>
        <div className="whrapper">
          {isAuthenticated && <NavbarMobile />}
          {isAuthenticated && <Navbar />}

          {routes}
          <Footer />
        </div>
      </Router>
    </AuthContext.Provider>
  );
}

export default App;
