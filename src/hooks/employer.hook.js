import { useState, useCallback } from "react";

const storageName = "employerData";

export const useEmployerInfo = () => {
  const [employerId, setEmployerId] = useState(null);

  const getEmployerId = useCallback((employerInfo) => {
    setEmployerId(employerInfo.id);

    localStorage.setItem(
      storageName,
      JSON.stringify({
        employerId: employerInfo.id,
      })
    );
  }, []);

  return { getEmployerId, employerId };
};
