import { useState, useEffect, useCallback } from "react";

const useErrors = (errorsForBanner) => {
  const [serverErrors, setServerErrors] = useState(null);
  const [errorMessage, setErrorMessage] = useState(null);

  const hideBannerHandler = useCallback((e) => {
    e.preventDefault();
    setServerErrors(null);
    setErrorMessage(null);
  }, []);

  useEffect(() => {
    const onErrorArray = (arr) => {
      arr.forEach(function (item) {
        if (item) {
          setErrorMessage(errorsForBanner(item));
        }
      });
    };
    if (serverErrors && Array.isArray(serverErrors)) {
      onErrorArray(serverErrors);
    } else if (serverErrors && serverErrors.Password) {
      onErrorArray(serverErrors.Password);
    } else if (serverErrors && serverErrors.Server) {
      setErrorMessage(errorsForBanner(serverErrors.Server));
    }
  }, [serverErrors, errorsForBanner]);

  return { errorMessage, setServerErrors, hideBannerHandler };
};

export default useErrors;
