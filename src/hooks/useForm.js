import { useState, useEffect, useCallback } from "react";

const useForm = (
  values,
  setValues,
  validate,
  valuesNumber,
  setValuesNumber
) => {
  const [errors, setErrors] = useState({});
  const [isSubmitting, setIsSubmitting] = useState(false);
  const [isSubmitted, setIsSubmitted] = useState(false);

  const submitForm = useCallback(() => {
    setIsSubmitted(true);
  }, []);

  const changeHandler = (e) => {
    const { target } = e;
    const value = target.type === "checkbox" ? target.checked : target.value;
    const { name } = target;
    setValues({
      ...values,
      [name]: value,
    });
  };

  const changeHandlerNumber = (e) => {
    const { name, value } = e.target;
    setValuesNumber({
      ...valuesNumber,
      [name]: Number(value),
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setErrors(validate(values, valuesNumber));
    setIsSubmitting(true);
  };

  const enterSubmit = (e) => {
    setErrors(validate(values, valuesNumber));
    setIsSubmitting(true);
  };

  useEffect(() => {
    if (Object.keys(errors).length === 0 && isSubmitting) {
      submitForm();
    }
  }, [errors, submitForm, isSubmitting]);

  return {
    handleSubmit,
    errors,
    isSubmitted,
    changeHandler,
    changeHandlerNumber,
    setIsSubmitted,
    enterSubmit,
  };
};

export default useForm;
