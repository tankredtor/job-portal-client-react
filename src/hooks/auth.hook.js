import { useState, useCallback, useEffect } from "react";

const storageName = "userData";

export const useAuth = () => {
  const [token, setToken] = useState(null);
  const [ready, setReady] = useState(false);
  const [username, setUsername] = useState(null);
  const [userRole, setUserRole] = useState(null);
  const [userId, setUserId] = useState(null);

  const login = useCallback((jwtToken, usernameField, role, id) => {
    setToken(jwtToken);
    setUsername(usernameField);
    setUserRole(role);
    setUserId(id);

    localStorage.setItem(
      storageName,
      JSON.stringify({
        username: usernameField,
        token: jwtToken,
        roles: role,
        userId: id,
      })
    );
  }, []);

  const logout = useCallback(() => {
    setToken(null);
    setUsername(null);
    setUserRole(null);
    setUserId(null);
    localStorage.clear();
  }, []);

  useEffect(() => {
    const data = JSON.parse(localStorage.getItem(storageName));

    if (data && data.token) {
      login(data.token, data.username, data.roles, data.userId);
    }

    setReady(true);
  }, [login]);

  return { login, logout, token, username, userRole, ready, userId };
};
