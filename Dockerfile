# Step 1
FROM node:10-alpine as build-step
RUN mkdir /app
WORKDIR /app
COPY package.json /app
RUN npm install
COPY . /app
ARG REACT_APP_API_URL
ENV REACT_APP_API_URL $REACT_APP_API_URL
ARG REACT_APP_API_KEY
ENV REACT_APP_API_KEY $REACT_APP_API_KEY
RUN npm run build
# Stage 22
FROM nginx:1.17.1-alpine
COPY ./util/nginx.conf /etc/nginx/nginx.conf
RUN rm -rf /usr/share/nginx/html/*
COPY --from=build-step /app/build /usr/share/nginx/html

#fdsg

